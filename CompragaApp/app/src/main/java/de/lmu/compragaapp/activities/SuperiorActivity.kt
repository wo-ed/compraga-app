package de.lmu.compragaapp.activities

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.MenuItem
import de.lmu.compragaapp.R
import de.lmu.compragaapp.entities.Superior
import de.lmu.compragaapp.fragments.*
import org.jetbrains.anko.intentFor

class SuperiorActivity : NavigationMainActivity(R.menu.activity_superior_drawer) {
    private val superior by lazy { intent.getSerializableExtra(INTENT_KEY_SUPERIOR) as Superior}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(savedInstanceState == null) {
            replaceMainContentFragment(createDefaultFragment())
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        super.onNavigationItemSelected(item)

        if(item.itemId == R.id.navSignOut) {
            finish()
            return true
        }

        val fragment: Fragment = when(item.itemId) {
            R.id.navTransactions -> TransactionsFragment.newInstance(superior)
            R.id.navRegisterEmployee ->
                EmployeeDataFragment.newInstance(EmployeeDataFragment.Mode.REGISTER, superior)
            else -> throw NoSuchElementException()
        }

        replaceMainContentFragment(fragment)
        return true
    }

    override fun createDefaultFragment() = TransactionsFragment.newInstance(superior)

    companion object {
        private const val INTENT_KEY_SUPERIOR = "INTENT_KEY_SUPERIOR"

        fun start(ctx: Context, superior: Superior) {
            ctx.startActivity(ctx.intentFor<SuperiorActivity>(INTENT_KEY_SUPERIOR to superior))
        }
    }
}