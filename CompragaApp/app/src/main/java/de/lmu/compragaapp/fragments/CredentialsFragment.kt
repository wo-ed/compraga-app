package de.lmu.compragaapp.fragments


import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import de.lmu.compragaapp.R
import de.lmu.compragaapp.Role
import de.lmu.compragaapp.activities.EmployeeActivity
import de.lmu.compragaapp.activities.NavigationMainActivity
import de.lmu.compragaapp.activities.ShopActivity
import de.lmu.compragaapp.activities.SuperiorActivity
import de.lmu.compragaapp.entities.Employee
import de.lmu.compragaapp.entities.Shop
import de.lmu.compragaapp.entities.Superior
import de.lmu.compragaapp.restservices.EmployeeCompragaService
import de.lmu.compragaapp.restservices.ShopCompragaService
import de.lmu.compragaapp.restservices.SuperiorCompragaService
import de.lmu.compragaapp.utils.hideKeyboard
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_credentials.*
import kotlinx.android.synthetic.main.loading_overlay.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.toast

class CredentialsFragment : Fragment(), AnkoLogger {
    private val role: Role by lazy { Role.valueOf(arguments.getString(ARGUMENT_ROLE)) }
    private val verifiedEmployee by lazy {
        arguments.getSerializable(ARGUMENT_VERIFIED_EMPLOYEE) as? Employee
    }

    private var subscription: Disposable? = null
    private val employeeService by lazy { EmployeeCompragaService.create() }
    private val shopService by lazy { ShopCompragaService.create() }
    private val superiorService by lazy { SuperiorCompragaService.create() }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_credentials, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        loginEmailText.setText(when(role) {
            Role.EMPLOYEE -> "philipp@compraga.de"
            Role.SHOP -> "edeka@shop.de"
            Role.SUPERIOR -> "uwewalter@compraga.de"
        })

        loginButton.setOnClickListener {
            activity.hideKeyboard()
            val email = loginEmailText.text.toString()
            if (email == "admin") {
                loginAsAdmin()
            } else if (email.isNotEmpty()) {
                login(email)
            }
        }

        credentialsHeader.text = when (role) {
            Role.EMPLOYEE -> getString(R.string.employee)
            Role.SUPERIOR -> getString(R.string.superior)
            Role.SHOP -> getString(R.string.shop)
        }
    }

    private fun login(email: String) {
        subscription?.dispose()
        when (role) {
            Role.SUPERIOR -> subscription = subscribe(superiorService.getSuperiorByEmail(email),
                    onNext = { SuperiorActivity.start(context, it) })

            Role.EMPLOYEE -> subscription = subscribe(employeeService.getEmployeeByEmail(email),
                    onNext = {
                        EmployeeActivity.start(context, it)
                    })

            Role.SHOP -> subscription = subscribe(shopService.getShopByEmail(email),
                    onNext = {
                        ShopActivity.start(context, it, verifiedEmployee)
                    })
        }
    }

    private fun loginAsAdmin() {
        when (role) {
            Role.SUPERIOR -> SuperiorActivity.start(context, Superior().apply {
                company = "Admin GmbH"
                email = "admin"
                firstname = "Admin"
                lastname = "Admin"
            })
            Role.EMPLOYEE -> EmployeeActivity.start(context, Employee().apply {
                this.cardnumber = 0
                this.company = "Admin GmbH"
                this.email = "admin"
                this.firstname = "Admin"
                this.lastname = "Admin"
                this.superioremail = "superior@admin"
            })
            Role.SHOP -> {
                ShopActivity.start(context, Shop().apply {
                    this.email = "admin"
                    this.name = "Admin's"
                }, verifiedEmployee)
            }
        }
    }

    fun <T : Any> subscribe(o: Observable<T>, onNext: (T) -> Unit): Disposable {
        setLoading(true)
        return o.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext,
                        onError = {
                            error("Could not verify!", it)
                            longToast(it.localizedMessage)
                            setLoading(false)
                        },
                        onComplete = {
                            setLoading(false)
                        }
                )
    }

    private fun setLoading(isLoading: Boolean) {
        loadingOverlay.visibility = if(isLoading) View.VISIBLE else View.GONE
    }

    override fun onPause() {
        super.onPause()
        setLoading(false)
        subscription?.dispose()
    }

    companion object {
        private const val ARGUMENT_ROLE = "ARGUMENT_ROLE"
        private const val ARGUMENT_VERIFIED_EMPLOYEE = "VERIFIED_EMPLOYEE"

        fun newInstance(role: Role) = CredentialsFragment().apply {
            arguments = Bundle().apply {
                putString(ARGUMENT_ROLE, role.name)
            }
        }

        fun newInstance(verifiedEmployee: Employee) = newInstance(Role.SHOP).apply {
            arguments.putSerializable(ARGUMENT_VERIFIED_EMPLOYEE, verifiedEmployee)
        }
    }
}
