package de.lmu.compragaapp.entities

import java.io.Serializable

class Article(
        var name: String? = null,
        var amount: Int = 0,
        var price: Double = 0.toDouble()): Serializable
