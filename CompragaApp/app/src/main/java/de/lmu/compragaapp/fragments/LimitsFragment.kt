package de.lmu.compragaapp.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.utils.ColorTemplate
import de.lmu.compragaapp.R
import de.lmu.compragaapp.activities.NavigationMainActivity
import de.lmu.compragaapp.restservices.CardCompragaService
import de.lmu.compragaapp.utils.getCompatColor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_limits.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.toast

class LimitsFragment : Fragment(), MainContent, AnkoLogger {
    override val title: String by lazy { getString(R.string.limits) }
    override val navMenuItemId: Int = R.id.navLimits

    private var subscription: Disposable? = null
    private lateinit var navigationActivity: NavigationMainActivity

    private val cardService by lazy { CardCompragaService.create() }

    private val cardNumber by lazy { arguments.getLong(ARGUMENT_CARD_NUMBER) }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_limits, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        loadLimits()
    }

    private fun loadLimits() {
        navigationActivity.setLoading(true)
        subscription?.dispose()
        subscription = cardService.getCardByNumber(cardNumber)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            initChart(it.limitCurrent, it.limitFix - it.limitCurrent)
                            transactionLimitEdit.text = "${it.limitTransaction} €"
                        },
                        onError = {
                            longToast(it.localizedMessage)
                            this.error("Limits could not be loaded", it)
                            navigationActivity.setLoading(false)
                        },
                        onComplete = {
                            navigationActivity.setLoading(false)
                        })
    }

    private fun initChart(amountRemaining: Int, amountSpent: Int) {
        val yVals = listOf(
                PieEntry(amountRemaining.toFloat(), getString(R.string.remaining)),
                PieEntry(amountSpent.toFloat(), getString(R.string.spent)))

        chart.data = PieData(PieDataSet(yVals, "amount").apply {
            colors = listOf(
                    getCompatColor(R.color.colorAmountRemaining),
                    getCompatColor(R.color.colorAmountSpent)
            )
            valueTextSize = 12f
            valueTextColor = ColorTemplate.rgb("#ffffff")
        })

        chart.apply {
            description.text = ""
            invalidate()
            setEntryLabelTextSize(12f)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        navigationActivity = context as? NavigationMainActivity ?: throw RuntimeException(
                "$context must implement ${NavigationMainActivity::class}"
        )
    }

    override fun onPause() {
        super.onPause()
        navigationActivity.setLoading(false)
        subscription?.dispose()
    }

    companion object {
        private const val ARGUMENT_CARD_NUMBER = "ARGUMENT_CARD_NUMBER"

        fun newInstance(cardNumber: Long) = LimitsFragment().apply {
            arguments = Bundle().apply {
                putLong(ARGUMENT_CARD_NUMBER, cardNumber)
            }
        }
    }
}
