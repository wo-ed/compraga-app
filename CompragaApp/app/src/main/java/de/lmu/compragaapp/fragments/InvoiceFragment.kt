package de.lmu.compragaapp.fragments


import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.pdf.PdfDocument
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import de.lmu.compragaapp.R
import de.lmu.compragaapp.activities.NavigationMainActivity
import de.lmu.compragaapp.adapters.ArticlesAdapter
import de.lmu.compragaapp.entities.Employee
import de.lmu.compragaapp.entities.Request.TransactionValidateRequest
import de.lmu.compragaapp.entities.Shop
import de.lmu.compragaapp.entities.Transaction
import de.lmu.compragaapp.restservices.TransactionCompragaService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_invoice.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.layoutInflater
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.toast
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class InvoiceFragment : Fragment(), MainContent, ArticlesAdapter.ClickListener, AnkoLogger {

    internal var rv: RecyclerView? = null
    private lateinit var transaction: Transaction
    private var employee: Employee? = null
    private var shop: Shop? = null
    private var subscription: Disposable? = null
    private val transactionService by lazy { TransactionCompragaService.create() }
    private var navigationActivity: NavigationMainActivity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        transaction = arguments.getSerializable(ARGUMENT_TRANSACTION) as Transaction
        employee = arguments.getSerializable(ARGUMENT_EMPLOYEE) as Employee
        shop = arguments.getSerializable(ARGUMENT_SHOP) as Shop
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater!!.inflate(R.layout.fragment_invoice, container, false)
        rv = view.findViewById(R.id.invoiceRecyclerView) as RecyclerView

        val companyTextView = view.findViewById(R.id.companyText) as TextView
        companyTextView.text = transaction.company

        transaction.articles.let { rv?.adapter = ArticlesAdapter(context, it, this) }

        val llm = LinearLayoutManager(activity)
        rv?.layoutManager = llm
        rv?.addItemDecoration(DividerItemDecoration(context, llm.orientation))

        val confirmButton = view.findViewById(R.id.confirmButton) as Button

        confirmButton.setOnClickListener {
            startLoading()
            createTransaction(transaction)
        }

        val rejectButton = view.findViewById(R.id.rejectButton) as Button
        rejectButton.setOnClickListener {
            navigationActivity?.returnToMainPage()
        }

        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateSumText()

        transactionDoneButton.setOnClickListener {
            navigationActivity?.returnToMainPage()
        }
    }

    private fun createTransaction(ta: Transaction) {
        val taValidate = TransactionValidateRequest()
        taValidate.cardnumber = ta.cardnumber
        taValidate.payload = ta.payload.toInt()

        startLoading()
        subscription?.dispose()
        subscription = transactionService.validateTransaction(taValidate)
                .flatMap {
                    if (it.isStatus == true) {
                        activity.runOnUiThread { validatedCheckBox.isChecked = true }
                        transactionService.createTransaction(ta)
                    } else Observable.error(RuntimeException(it.message))
                }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            toast("Transaction created! Payload: " + it.payload.toString())
                            performedCheckBox.isChecked = true
                            transactionDoneButton.visibility = View.VISIBLE
                            progressBar.visibility = View.GONE

                            showInvoiceMessage()
                        },
                        onError = {
                            error("Could not validate transaction!", it)
                            longToast(it.localizedMessage)
                            cancelLoading()
                        }
                )
    }

    private fun startLoading() {
        performTransactionOverlay.visibility = View.VISIBLE
        progressBar.visibility = View.VISIBLE
    }

    private fun cancelLoading() {
        performTransactionOverlay.visibility = View.GONE
    }

    private fun showInvoiceMessage(){
        val builder = AlertDialog.Builder(activity)
        val inflater = activity.layoutInflater

        builder.setTitle("Save invoice?")
        val layout = inflater.inflate(R.layout.save_invoice_message, null)
        builder.setView(layout)
        builder.create()
        val dialog = builder.show()

        layout.findViewById(R.id.saveInvoiceYesButton).setOnClickListener {
            val pageNumber = 1
            val container = this.view?.parent as ViewGroup
            val pdfFile = try {
                createPdfFile()
            } catch (ex: Exception) {
                toast("Could not create pdf file")
                null
            }

            pdfFile?.let {
                val pdfView = createInvoicePdfView(container)
                createPdf(pdfView, it, pageNumber)
                showPdf(it)
            }

            dialog.dismiss()

            navigationActivity?.returnToMainPage()
        }

        layout.findViewById(R.id.saveInvoiceNoButton).setOnClickListener {
            dialog.dismiss()

            navigationActivity?.returnToMainPage()
        }
    }

    @Throws(IOException::class)
    private fun createPdfFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "PDF_${timeStamp}_"
        val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)
        return File.createTempFile(imageFileName, ".pdf", storageDir)
    }

    private fun showPdf(pdfFile: File){
        val path = Uri.fromFile(pdfFile)
        val pdfIntent = Intent(Intent.ACTION_VIEW)
        pdfIntent.setDataAndType(path, "application/pdf")
        
        try {
            startActivity(pdfIntent)
        }catch (e:Exception)
        {
            error("Could not display pdf", e)
            toast("Can´t display Pdf! Please install a PdfViewer")
        }
    }

    private fun createPdf(pageContent: View, file: File, pageNumber: Int) {
        if (android.os.Build.VERSION.SDK_INT >= 19) {

            val rvPdf = pageContent.findViewById(R.id.invoiceRecyclerViewPdf) as RecyclerView
            val pageHeight = pageContent.height + rvPdf.computeVerticalScrollRange()
            val pdf = PdfDocument()
            //val pageInfo = PdfDocument.PageInfo.Builder(pageContent.width, pageContent.height, pageNumber).create()
            val pageInfo = PdfDocument.PageInfo.Builder(pageContent.width, pageHeight, 1).create()
            val page: PdfDocument.Page = pdf.startPage(pageInfo)

            val content: View = pageContent
            content.draw(page.canvas)

            pdf.finishPage(page)

            FileOutputStream(file).use { fos ->
                try {
                    pdf.writeTo(fos)
                    toast("Pdf created!")
                } catch (e: IOException) {
                    toast("Error in createPdf!")
                    error("Could not create pdf", e)
                } finally {
                    try { pdf.close() }
                    catch(ex: Exception) {}
                }
            }
        } else {
            toast("Pdf creation not supported!")
        }
    }

    private fun createInvoicePdfView(container: ViewGroup?): View {
        var pdfView = context.layoutInflater.inflate(R.layout.fragment_invoice_pdf, null)

        var invoiceRecyclerViewPdf = pdfView.findViewById(R.id.invoiceRecyclerViewPdf) as RecyclerView
        transaction.articles.let { invoiceRecyclerViewPdf.adapter = ArticlesAdapter(context, it, this) }

        var companyTextView = pdfView.findViewById(R.id.companyTextPdf) as TextView
        companyTextView.text = transaction.company
        if( companyTextView.text.isEmpty())
            companyTextView.text = "Company"

        var dateTextView = pdfView.findViewById(R.id.datePdf) as TextView
        dateTextView.text = SimpleDateFormat.getDateInstance().format(Date())
        if( dateTextView.text.isNullOrEmpty() || dateTextView.text == "null")
            dateTextView.text = "Date"

        var totalTextView = pdfView.findViewById(R.id.totalSumTextInvoicePdf) as TextView
        totalTextView.text = "Total:" + transaction.payload.toString()

        val llm = LinearLayoutManager(activity)
        invoiceRecyclerViewPdf?.layoutManager = llm
        invoiceRecyclerViewPdf?.addItemDecoration(DividerItemDecoration(context, llm.orientation))

        pdfView.measure(container!!.width, container.height)
        pdfView.layout(0, 0, container.width, container.height)

        return pdfView
    }

    private fun updateSumText() {
        val sumText = view?.findViewById(R.id.totalSumTextInvoice) as TextView
        if (transaction.articles.isNotEmpty()) {
            val sumString: String = "Total " + transaction.payload.toString()
            sumText.text = sumString
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        navigationActivity = context as? NavigationMainActivity ?: throw RuntimeException(
                "$context must implement ${NavigationMainActivity::class}"
        )
    }

    override fun onDetach() {
        super.onDetach()
        navigationActivity = null
    }

    override fun onPause() {
        super.onPause()
        subscription?.dispose()
        cancelLoading()
    }

    override val title: String?
        get() = getString(R.string.invoice)

    override val navMenuItemId: Int?
        get() = null

    override fun onItemClick(holder: ArticlesAdapter.ViewHolder) {

    }

    override fun onItemLongClick(holder: ArticlesAdapter.ViewHolder) {

    }

    companion object {
        private const val ARGUMENT_TRANSACTION = "ARGUMENT_TRANSACTION"
        private const val ARGUMENT_EMPLOYEE = "ARGUMENT_EMPLOYEE"
        private const val ARGUMENT_SHOP = "ARGUMENT_SHOP"

        fun newInstance(transaction: Transaction, employee: Employee, shop: Shop) = InvoiceFragment().apply {
            arguments = Bundle().apply {
                putSerializable(ARGUMENT_TRANSACTION, transaction)
                putSerializable(ARGUMENT_EMPLOYEE, employee)
                putSerializable(ARGUMENT_SHOP, shop)
            }
        }
    }
}
