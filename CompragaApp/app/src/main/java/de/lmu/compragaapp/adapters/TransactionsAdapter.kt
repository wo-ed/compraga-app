package de.lmu.compragaapp.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import de.lmu.compragaapp.R
import de.lmu.compragaapp.entities.Transaction
import org.jetbrains.anko.find
import org.jetbrains.anko.layoutInflater
import java.text.DateFormat
import java.text.SimpleDateFormat

class TransactionsAdapter(
        private val context: Context,
        private val transactions: MutableList<Transaction>,
        private val clickListener: ClickListener?)
    : RecyclerView.Adapter<TransactionsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int) =
        ViewHolder(context.layoutInflater.inflate(R.layout.list_item_transaction, parent, false))

    override fun getItemCount() = transactions.size

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bind(transactions[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val dateView = itemView.find<TextView>(R.id.dateView)
        private val amountView = itemView.find<TextView>(R.id.amountView)
        private val namesView = itemView.find<TextView>(R.id.namesView)

        lateinit var transaction: Transaction

        init {
            itemView.setOnClickListener { clickListener?.onTransactionClick(this) }
        }

        fun bind(transaction: Transaction) {
            this.transaction = transaction

            amountView.text = "${transaction.payload} €"
            dateView.text = transaction.transactiondate?.let(dateFormat::format)

            val articles = transaction.articles
            namesView.text = when(articles.size) {
                0 -> context.getString(R.string.no_articles)
                1 -> articles.first().name
                else -> context.getString(R.string.articles_enumeration, articles.first().name, articles.size - 1)
            }
        }
    }

    interface ClickListener {
        fun onTransactionClick(holder: ViewHolder)
    }

    companion object {
        val dateFormat: DateFormat = SimpleDateFormat.getDateInstance()
    }
}