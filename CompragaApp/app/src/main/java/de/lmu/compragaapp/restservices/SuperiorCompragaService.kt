package de.lmu.compragaapp.restservices

import de.lmu.compragaapp.buildCompragaRetrofit
import de.lmu.compragaapp.entities.Superior
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface SuperiorCompragaService {

    @GET("superior/email={email}/")
    fun getSuperiorByEmail(@Path("email") email:String): Observable<Superior>

    @GET("superior/id={id}")
    fun getSuperiorById(@Path("id") id: Long): Observable<Superior>

    companion object {
        fun create(): SuperiorCompragaService =
                buildCompragaRetrofit().create(SuperiorCompragaService::class.java)
    }
}