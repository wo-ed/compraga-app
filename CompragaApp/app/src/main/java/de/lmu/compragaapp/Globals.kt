package de.lmu.compragaapp

const val EMULATOR_HOST_URL = "http://10.0.2.2:8080"
const val SERVER_URL = "http://141.84.213.224:3000"
val compragaServiceBaseUrl = EMULATOR_HOST_URL

enum class Role {
    SUPERIOR,
    EMPLOYEE,
    SHOP
}