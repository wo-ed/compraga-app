package de.lmu.compragaapp.entities.Response


class TransactionValidateResponse {
    var isStatus: Boolean? = null
    var message: String? = null
}
