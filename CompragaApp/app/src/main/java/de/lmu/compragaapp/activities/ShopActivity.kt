package de.lmu.compragaapp.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import de.lmu.compragaapp.R
import de.lmu.compragaapp.entities.Employee
import de.lmu.compragaapp.entities.Shop
import de.lmu.compragaapp.fragments.PerformTransactionFragment
import de.lmu.compragaapp.fragments.TransactionsFragment
import org.jetbrains.anko.intentFor

class ShopActivity : NavigationMainActivity(R.menu.activity_shop_drawer) {
    private val shop by lazy { intent.getSerializableExtra(INTENT_KEY_SHOP) as Shop }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            replaceMainContentFragment(createDefaultFragment())
        }

        val verifiedEmployee = intent.getSerializableExtra(INTENT_KEY_VERIFIED_EMPLOYEE) as? Employee
        intent.removeExtra(INTENT_KEY_VERIFIED_EMPLOYEE)

        verifiedEmployee?.let {
            replaceMainContentFragment(
                    PerformTransactionFragment.newInstance(shop, verifiedEmployee),
                    addToBackStack = true
            )
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        super.onNavigationItemSelected(item)

        when (item.itemId) {
            R.id.navSignOut -> finish()
            R.id.navVerifyCard ->
                startActivityForResult(intentFor<VerifyCardActivity>(), REQUEST_VERIFY_CARD)
            R.id.navTransactions ->
                replaceMainContentFragment(TransactionsFragment.newInstance(shop))

            /*
        // TODO: Remove perform employee data nav item
            R.id.navPerformTransaction ->
                replaceMainContentFragment(PerformTransactionFragment.newInstance(shop, Employee()),
                        addToBackStack = true)*/

            else -> throw NoSuchElementException()
        }

        return true
    }

    override fun createDefaultFragment() = TransactionsFragment.newInstance(shop)

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == LoginActivity.REQUEST_VERIFY_CARD && resultCode == Activity.RESULT_OK) {
            val employee = data?.getSerializableExtra(VerifyCardActivity.RESULT_EMPLOYEE) as Employee
            replaceMainContentFragment(PerformTransactionFragment.newInstance(shop, employee),
                    addToBackStack = true)
        }
    }

    companion object {
        private const val INTENT_KEY_SHOP = "INTENT_KEY_SHOP"
        private const val INTENT_KEY_VERIFIED_EMPLOYEE = "INTENT_KEY_VERIFIED_EMPLOYEE"

        private const val REQUEST_VERIFY_CARD = 1

        fun start(ctx: Context, shop: Shop, verifiedEmployee: Employee? = null) {
            ctx.startActivity(ctx.intentFor<ShopActivity>(
                    INTENT_KEY_SHOP to shop,
                    INTENT_KEY_VERIFIED_EMPLOYEE to verifiedEmployee
            ))
        }
    }
}