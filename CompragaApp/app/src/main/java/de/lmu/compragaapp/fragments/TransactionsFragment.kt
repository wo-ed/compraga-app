package de.lmu.compragaapp.fragments

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import de.lmu.compragaapp.R
import de.lmu.compragaapp.activities.NavigationMainActivity
import de.lmu.compragaapp.activities.SearchEmployeeActivity
import de.lmu.compragaapp.adapters.TransactionsAdapter
import de.lmu.compragaapp.entities.Employee
import de.lmu.compragaapp.entities.Shop
import de.lmu.compragaapp.entities.Superior
import de.lmu.compragaapp.entities.Transaction
import de.lmu.compragaapp.restservices.TransactionCompragaService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_transactions.*
import org.jetbrains.anko.*
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.toast

class TransactionsFragment : Fragment(), MainContent, TransactionsAdapter.ClickListener,
        AnkoLogger {

    override val title: String by lazy { getString(R.string.transactions) }
    override val navMenuItemId: Int = R.id.navTransactions

    private var navigationActivity: NavigationMainActivity? = null

    private var checkedRadioButtonId: Int = R.id.radioButtonTotal

    private var subscription: Disposable? = null
    private var allTransactions = listOf<Transaction>()
    private val filteredTransactions = mutableListOf<Transaction>()
    private val transactionService by lazy { TransactionCompragaService.create() }

    private val shop by lazy { arguments.getSerializable(ARGUMENT_SHOP) as? Shop }
    private val employee by lazy { arguments.getSerializable(ARGUMENT_EMPLOYEE) as? Employee }
    private val superior by lazy { arguments.getSerializable(ARGUMENT_SUPERIOR) as? Superior }

    private var employeeFilter: Employee? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_transactions, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        savedInstanceState?.let {
            checkedRadioButtonId = it.getInt(STATE_CHECKED_RADIO_BUTTON_ID)
            employeeFilter = it.getSerializable(STATE_EMPLOYEE_FILTER) as? Employee
        }

        transactionsView.apply {
            adapter = TransactionsAdapter(context, filteredTransactions, this@TransactionsFragment)
            val orientation = LinearLayoutManager.VERTICAL
            this.layoutManager = LinearLayoutManager(context, orientation, false)
            addItemDecoration(DividerItemDecoration(context, orientation))
        }

        val dataSet1 = BarDataSet(listOf(BarEntry(50f, 10f), BarEntry(70f, 20f),
                BarEntry(90f, 100f), BarEntry(100f, 90f)), "Transaktionsbeträge")

        chart.apply {
            data = BarData(dataSet1)
            description.text = ""
        }

        periodButton.setOnClickListener { showChoosePeriodDialog() }

        superior?.let { s ->
            employeeButton.visibility = View.VISIBLE
            employeeLabel.visibility = View.VISIBLE

            employeeButton.setOnClickListener {
                if (employeeFilter != null) {
                    employeeFilter = null
                    employeeButton.text = getString(R.string.all)
                    updateFilteredTransactions()
                } else {
                    startActivityForResult(context.intentFor<SearchEmployeeActivity>(
                            SearchEmployeeActivity.INTENT_KEY_SUPERIOR to s), REQUEST_CHOOSE_EMPLOYEE)
                }
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CHOOSE_EMPLOYEE && resultCode == Activity.RESULT_OK) {
            employeeFilter = data?.getSerializableExtra(SearchEmployeeActivity.INTENT_KEY_EMPLOYEE)
                    as Employee?
        }
    }

    private fun showChoosePeriodDialog() {
        val dialog = alert(R.string.choose_time_frame, R.string.period) {
            customView = context.layoutInflater.inflate(R.layout.dialog_filter_transactions,
                    transactionsCoordinatorLayout, false)
            //filterRadioGroup.check(checkedRadioButtonId)
            okButton {
                //checkedRadioButtonId = filterRadioGroup.checkedRadioButtonId
            }
            cancelButton {}
        }.show() as Dialog

        dialog.find<Button>(R.id.chooseDateButton).setOnClickListener {

        }
    }

    private fun loadTransactions() {
        val observable = employee?.cardnumber?.let(transactionService::getTransactionsByCardNumber) ?:
                superior?.company?.let(transactionService::getTransactionByCompanyName) ?:
                shop?.name?.let(transactionService::getTransactionByShopName)

        navigationActivity?.setLoading(true)
        subscription?.dispose()
        subscription = observable
                ?.subscribeOn(Schedulers.newThread())
                ?.observeOn(AndroidSchedulers.mainThread())
                ?.subscribeBy(
                        onNext = {
                            allTransactions = it.sortedByDescending { it.transactiondate }
                            updateFilteredTransactions()
                        },
                        onError = {
                            error("Transactions could not be retrieved", it)
                            longToast(it.localizedMessage);
                            navigationActivity?.setLoading(false)
                        },
                        onComplete = {
                            navigationActivity?.setLoading(false)
                        }
                )
    }

    private fun updateFilteredTransactions() {
        val filteredTransactions = employeeFilter?.let { employee ->
            allTransactions.filter { ta -> ta.cardnumber == employee.cardnumber }
        } ?: allTransactions

        this.filteredTransactions.clear()
        this.filteredTransactions.addAll(filteredTransactions)
        transactionsView.adapter.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        loadTransactions()

        employeeButton.text = employeeFilter?.let {
            "${employeeFilter?.firstname} ${employeeFilter?.lastname}"
        } ?: getString(R.string.all)
    }

    override fun onPause() {
        super.onPause()
        subscription?.dispose()
        navigationActivity?.setLoading(false)
    }

    override fun onDetach() {
        super.onDetach()
        navigationActivity = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        navigationActivity = context as? NavigationMainActivity ?: throw RuntimeException(
                "$context must implement ${NavigationMainActivity::class}"
        )
    }

    override fun onTransactionClick(holder: TransactionsAdapter.ViewHolder) {
        navigationActivity?.replaceMainContentFragment(
                TransactionDetailFragment.newInstance(holder.transaction),
                addToBackStack = true)
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.putInt(STATE_CHECKED_RADIO_BUTTON_ID, checkedRadioButtonId)
        outState?.putSerializable(STATE_EMPLOYEE_FILTER, employeeFilter)
    }

    companion object {
        private const val ARGUMENT_SUPERIOR = "ARGUMENT_SUPERIOR"
        private const val ARGUMENT_EMPLOYEE = "ARGUMENT_EMPLOYEE"
        private const val ARGUMENT_SHOP = "ARGUMENT_SHOP"

        private const val REQUEST_CHOOSE_EMPLOYEE = 1

        private const val STATE_CHECKED_RADIO_BUTTON_ID = "STATE_CHECKED_RADIO_BUTTON_ID"
        private const val STATE_EMPLOYEE_FILTER = "STATE_EMPLOYEE_FILTER"

        fun newInstance(superior: Superior) = TransactionsFragment().apply {
            arguments = Bundle().apply {
                putSerializable(ARGUMENT_SUPERIOR, superior)
            }
        }

        fun newInstance(shop: Shop) = TransactionsFragment().apply {
            arguments = Bundle().apply {
                putSerializable(ARGUMENT_SHOP, shop)
            }
        }

        fun newInstance(employee: Employee) = TransactionsFragment().apply {
            arguments = Bundle().apply {
                putSerializable(ARGUMENT_EMPLOYEE, employee)
            }
        }
    }
}
