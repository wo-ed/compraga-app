package de.lmu.compragaapp.fragments

import android.graphics.Bitmap
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import de.lmu.compragaapp.R
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_generate_qrcode.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.support.v4.longToast

class GenerateQRCodeFragment : Fragment(), AnkoLogger, MainContent {
    override val title: String by lazy { getString(R.string.generate_qr_code) }
    override val navMenuItemId: Int = R.id.navGenerateQRCode

    private var subscription: Disposable? = null

    private val qrString by lazy { arguments.getString(ARGUMENT_QR_STRING) }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_generate_qrcode, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cardNumberView.text = qrString
    }

    @Throws(WriterException::class, IllegalArgumentException::class)
    fun generateQrCode(qr: String): Observable<Bitmap> {
        return Observable.fromCallable {
            val result = MultiFormatWriter().encode(qr, BarcodeFormat.QR_CODE, SIZE, SIZE, null)

            val w = result.width
            val h = result.height
            val pixels = IntArray(w * h)
            for (y in 0..h - 1) {
                val offset = y * w
                for (x in 0..w - 1) {
                    pixels[offset + x] = if (result.get(x, y)) Color.BLACK else Color.WHITE
                }
            }

            Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888).apply {
                setPixels(pixels, 0, SIZE, 0, 0, w, h)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        subscription = generateQrCode(qrString)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = qrCodeView::setImageBitmap,
                        onError = {
                            error("QR code could not be generated", it)
                            longToast(it.localizedMessage)
                        }
                )
    }

    override fun onPause() {
        super.onPause()
        subscription?.dispose()
    }

    companion object {
        private const val SIZE = 500

        private const val ARGUMENT_QR_STRING = "bundleKeyQRString"

        fun newInstance(qrString: String) = GenerateQRCodeFragment().apply {
            arguments = Bundle().apply {
                putString(ARGUMENT_QR_STRING, qrString)
            }
        }
    }
}
