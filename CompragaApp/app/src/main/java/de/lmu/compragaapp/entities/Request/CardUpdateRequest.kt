package de.lmu.compragaapp.entities.Request

class CardUpdateRequest {
    var number: Long = 0
    var emailAddress: String? = null
    var limitTransaction: Int = 0
    var limitCurrent: Int = 0
    var limitFix: Int = 0
    var imageHash: String? = null
}
