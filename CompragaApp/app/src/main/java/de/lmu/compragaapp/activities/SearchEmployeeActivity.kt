package de.lmu.compragaapp.activities


import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.View
import de.lmu.compragaapp.R
import de.lmu.compragaapp.adapters.EmployeeAdapter
import de.lmu.compragaapp.entities.Employee
import de.lmu.compragaapp.entities.Superior
import de.lmu.compragaapp.restservices.EmployeeCompragaService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_search_employee.*
import kotlinx.android.synthetic.main.loading_overlay.*
import org.jetbrains.anko.*


class SearchEmployeeActivity : AppCompatActivity(), AnkoLogger, EmployeeAdapter.ClickListener {

    private var allEmployees = listOf<Employee>()
    private val filteredEmployees = ArrayList<Employee>()
    private var subscription: Disposable? = null
    private val employeeService by lazy {EmployeeCompragaService.create()}
    private val superior by lazy { intent.getSerializableExtra(INTENT_KEY_SUPERIOR) as? Superior }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_employee)

        searchEmployeeRecyclerView.apply {
            adapter = EmployeeAdapter(filteredEmployees, this@SearchEmployeeActivity)
            val orientation = LinearLayoutManager.VERTICAL
            layoutManager = LinearLayoutManager(this@SearchEmployeeActivity, orientation, false)
            addItemDecoration(DividerItemDecoration(context, orientation))
        }
    }

    private fun loadEmployees()
    {
        val company = superior?.company ?: return

        setLoading(true)
        subscription?.dispose()
        subscription = employeeService.getEmployeesByCompany(company)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            allEmployees = it
                            filteredEmployees.clear()
                            filteredEmployees.addAll(it)
                            searchEmployeeRecyclerView.adapter.notifyDataSetChanged()
                        },
                        onError = {
                            error("Could not load employees", it)
                            longToast(it.localizedMessage)
                            setLoading(false)
                        },
                        onComplete = {
                            setLoading(false)
                        }
                )
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu, menu)

        val searchView = menu?.findItem(R.id.searchBar)?.actionView as SearchView?
        searchView?.isIconified = true

        searchView?.setOnQueryTextListener( object: SearchView.OnQueryTextListener{

            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                val tokens = newText?.split(" ")
                val searchList = if(tokens?.isNotEmpty() == true)
                    allEmployees.filter { employee ->
                        tokens.filter { token ->
                            employee.firstname?.toLowerCase()?.contains(token) == true ||
                                    employee.lastname?.toLowerCase()?.contains(token) == true
                        }.isNotEmpty()
                    } else allEmployees

                // Update employeelist
                filteredEmployees.clear()
                filteredEmployees.addAll(searchList)
                searchEmployeeRecyclerView.adapter.notifyDataSetChanged()

                return true
            }
        })

        return true
    }

    fun setLoading(isLoading: Boolean) {
        loadingOverlay.visibility = if(isLoading) View.VISIBLE else View.GONE
    }

    override fun onEmployeeClick(holder: EmployeeAdapter.ViewHolder) {
        val employee = filteredEmployees[holder.adapterPosition]
        setResult(Activity.RESULT_OK, Intent().putExtra(INTENT_KEY_EMPLOYEE, employee))
        finish()
    }

    override fun onPause() {
        super.onPause()
        setLoading(false)
    }

    override fun onResume() {
        super.onResume()
        loadEmployees()
    }

    companion object {
        const val INTENT_KEY_SUPERIOR = "INTENT_KEY_SUPERIOR"

        const val INTENT_KEY_EMPLOYEE = "INTENT_KEY_EMPLOYEE"
    }
}
