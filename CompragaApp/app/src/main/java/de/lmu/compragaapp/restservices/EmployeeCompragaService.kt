package de.lmu.compragaapp.restservices

import de.lmu.compragaapp.buildCompragaRetrofit
import de.lmu.compragaapp.entities.Employee
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface EmployeeCompragaService {

    @GET("employee/lastname={lastname}")
    fun getEmployees(@Path("lastname") lastname: String): Observable<List<Employee>>

    @GET("employee/email={email}/")
    fun getEmployeeByEmail(@Path("email") email: String): Observable<Employee>

    @GET("employee/cardnumber={cardnumber}")
    fun getEmployeeByCardnumber(@Path("cardnumber") cardnumber: Long): Observable<Employee>

    @GET("employee/id={id}")
    fun getEmployeesById(@Path("id") id: Long): Observable<Employee>

    @GET("employee")
    fun getAllEmployees(): Observable<List<Employee>>

    @GET("employee/company={company}")
    fun getEmployeesByCompany(@Path("company") company: String): Observable<List<Employee>>

    @POST("employee/create")
    fun addEmployee(@Body employee: Employee): Observable<Employee>

    companion object {
        fun create(): EmployeeCompragaService =
                buildCompragaRetrofit().create(EmployeeCompragaService::class.java)
    }
}