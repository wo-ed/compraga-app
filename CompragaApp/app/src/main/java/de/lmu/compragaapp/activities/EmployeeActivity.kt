package de.lmu.compragaapp.activities

import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.Fragment
import android.view.MenuItem
import de.lmu.compragaapp.R
import de.lmu.compragaapp.Role
import de.lmu.compragaapp.entities.Employee
import de.lmu.compragaapp.fragments.*
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.toast

class EmployeeActivity : NavigationMainActivity(R.menu.activity_employee_drawer) {
    private val employee by lazy { intent.getSerializableExtra(INTENT_KEY_EMPLOYEE) as Employee }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if(savedInstanceState == null) {
            replaceMainContentFragment(createDefaultFragment())
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        super.onNavigationItemSelected(item)

        if(item.itemId == R.id.navSignOut) {
            finish()
            return true
        }

        val fragment: Fragment = when(item.itemId) {
            R.id.navEmployeeData -> EmployeeDataFragment.newInstance(employee)
            R.id.navTransactions -> TransactionsFragment.newInstance(employee)
            R.id.navLimits -> LimitsFragment.newInstance(employee.cardnumber)
            R.id.navGenerateQRCode -> GenerateQRCodeFragment.newInstance(employee.cardnumber.toString())
            else -> throw NoSuchElementException()
        }

        replaceMainContentFragment(fragment)
        return true
    }

    override fun createDefaultFragment() = TransactionsFragment.newInstance(employee)

    companion object {
        private const val INTENT_KEY_EMPLOYEE = "INTENT_KEY_EMPLOYEE"

        fun start(ctx: Context, employee: Employee) {
            ctx.startActivity(ctx.intentFor<EmployeeActivity>(INTENT_KEY_EMPLOYEE to employee))
        }
    }
}