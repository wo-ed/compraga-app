package de.lmu.compragaapp.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.google.zxing.integration.android.IntentIntegrator
import de.lmu.compragaapp.R
import de.lmu.compragaapp.entities.Employee
import de.lmu.compragaapp.fragments.VerifyImageFragment
import de.lmu.compragaapp.restservices.CardCompragaService
import de.lmu.compragaapp.restservices.EmployeeCompragaService
import de.lmu.compragaapp.utils.hash
import de.lmu.compragaapp.utils.hideKeyboard
import de.lmu.compragaapp.utils.startScanActivity
import de.lmu.compragaapp.utils.toBase64
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_verify_card.*
import kotlinx.android.synthetic.main.loading_overlay.*
import org.jetbrains.anko.*

class VerifyCardActivity : AppCompatActivity(), AnkoLogger,
        VerifyImageFragment.OnFragmentInteractionListener {

    private val cardService by lazy { CardCompragaService.create() }
    private val employeeService by lazy { EmployeeCompragaService.create() }
    private var subscription: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_card)

        cardNumberButton.setOnClickListener {
            hideKeyboard()
            cardNumberView.text.toString().trim().toLongOrNull()?.let(this::loadEmployeeData)
        }

        scanButton.setOnClickListener { startScanActivity() }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        result?.contents?.toLongOrNull()?.let(this::loadEmployeeData)
    }

    override fun onPause() {
        super.onPause()
        subscription?.dispose()
        setLoading(false)
    }

    private fun setLoading(isLoading: Boolean) {
        loadingOverlay.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    fun loadEmployeeData(cardNumber: Long) {
        setLoading(true)

        subscription = cardService.getCardByNumber(cardNumber)
                .flatMap { cardResponse ->
                    employeeService.getEmployeeByCardnumber(cardNumber).map { employee ->
                        cardResponse to employee
                    }
                }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = { (card, employee) ->
                            val employeeImageHash = employee.image?.hash()?.toBase64()
                            val cardImageHash = card.imageHash

                            if (employeeImageHash?.isNotBlank() == true &&
                                    employeeImageHash == cardImageHash) {
                                onHashEqualityTestSucceeded(cardImageHash, employeeImageHash, employee)
                            } else {
                                onHashEqualityTestFailed(cardImageHash, employeeImageHash)
                            }
                        },
                        onError = {
                            longToast(it.localizedMessage)
                            error("Could not retrieve image", it)
                            setLoading(false)
                        },
                        onComplete = {
                            setLoading(false)
                        }
                )
    }


    private fun onHashEqualityTestFailed(cardImageHash: String?, employeeImageHash: String?) {
        alert("Generated\n$employeeImageHash\n\nBlockchain\n$cardImageHash", "Image hashes are not equal") {
            okButton { }
        }.show()
    }

    private fun onHashEqualityTestSucceeded(cardImageHash: String?, employeeImageHash: String?, employee: Employee) {
        alert("Generated\n$employeeImageHash\n\nBlockchain\n$cardImageHash", "Image hashes are equal") {
            okButton {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.verifyEmployeeFragmentContainer,
                                VerifyImageFragment.newInstance(employee))
                        .addToBackStack(null)
                        .commit()
            }
        }.show()
    }

    override fun onImageVerificationAccepted(employee: Employee) {
        setResult(Activity.RESULT_OK,
                Intent().putExtra(VerifyCardActivity.RESULT_EMPLOYEE, employee))
        finish()
    }

    override fun onImageVerificationCancelled(employee: Employee) {
        supportFragmentManager.popBackStack()
    }

    companion object {
        const val RESULT_EMPLOYEE = "RESULT_EMPLOYEE"
    }
}
