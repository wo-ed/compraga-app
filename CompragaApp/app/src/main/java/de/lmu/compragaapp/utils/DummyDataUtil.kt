package de.lmu.compragaapp.utils

import de.lmu.compragaapp.entities.Article
import de.lmu.compragaapp.entities.Transaction
import io.reactivex.Observable
import java.math.BigDecimal
import java.sql.Date
import java.util.*

object DummyDataUtil {
    private val random = Random()

    fun dummyArticles() = (0..random.nextInt(999)).map {
        Article("Dummy article", random.nextInt(9999), random.nextInt(999).toDouble())
    }

    fun dummyTransactions(): Observable<List<Transaction>> =
            Observable.fromCallable { (0..100).map { dummyTransaction() } }

    fun dummyTransaction(): Transaction =
            Transaction().apply {
                shopname = "My shop"
                company = "My company"
                transactiondate = Date(Calendar.getInstance().timeInMillis)
                articles = dummyArticles()
            }
}