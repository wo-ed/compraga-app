package de.lmu.compragaapp.entities

import java.io.Serializable

class Shop: Serializable{
    var id: Long = 0
    var name: String? = null
    var email: String? = null
}