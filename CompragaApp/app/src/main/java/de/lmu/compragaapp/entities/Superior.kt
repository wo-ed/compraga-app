package de.lmu.compragaapp.entities

import java.io.Serializable

class Superior: Serializable {
    var id: Long = 0
    var firstname: String? = null
    var lastname: String? = null
    var email: String? = null
    var company: String? = null
}
