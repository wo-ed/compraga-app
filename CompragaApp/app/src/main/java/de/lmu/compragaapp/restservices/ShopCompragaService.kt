package de.lmu.compragaapp.restservices

import de.lmu.compragaapp.buildCompragaRetrofit
import de.lmu.compragaapp.entities.Shop
import de.lmu.compragaapp.entities.Transaction
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface ShopCompragaService {

    @GET("shop/email={email}/")
    fun getShopByEmail(@Path("email") email: String ): Observable<Shop>

    @GET("shop/id={id}")
    fun getShopById(@Path("id") id: Long): Observable<Shop>

    companion object {
        fun create(): ShopCompragaService =
                buildCompragaRetrofit().create(ShopCompragaService::class.java)
    }
}