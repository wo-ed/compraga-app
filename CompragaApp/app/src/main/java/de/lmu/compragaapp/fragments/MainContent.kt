package de.lmu.compragaapp.fragments

interface MainContent {
    val title: String?
    val navMenuItemId: Int?
}