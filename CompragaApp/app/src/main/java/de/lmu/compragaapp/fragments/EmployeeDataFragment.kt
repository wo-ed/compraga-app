package de.lmu.compragaapp.fragments

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.view.*
import de.lmu.compragaapp.R
import de.lmu.compragaapp.activities.NavigationMainActivity
import de.lmu.compragaapp.entities.Employee
import de.lmu.compragaapp.entities.Request.CardCreateRequest
import de.lmu.compragaapp.entities.Response.CardResponse
import de.lmu.compragaapp.entities.Superior
import de.lmu.compragaapp.restservices.CardCompragaService
import de.lmu.compragaapp.restservices.EmployeeCompragaService
import de.lmu.compragaapp.utils.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.container_edit_employee.*
import kotlinx.android.synthetic.main.container_view_employee.*
import kotlinx.android.synthetic.main.fragment_employee_data.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.error
import org.jetbrains.anko.imageBitmap
import org.jetbrains.anko.okButton
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.longToast
import org.jetbrains.anko.support.v4.toast
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class EmployeeDataFragment : Fragment(), MainContent, AnkoLogger {

    private val mode by lazy { Mode.valueOf(arguments.getString(ARGUMENT_MODE)) }
    private val superior by lazy { arguments.getSerializable(ARGUMENT_SUPERIOR) as Superior? }
    private val employee by lazy { arguments.getSerializable(ARGUMENT_EMPLOYEE) as Employee? }

    private var navigationActivity: NavigationMainActivity? = null
    private var tempPhotoUri: Uri? = null
    private var photoUri: Uri? = null
    private var subscription: Disposable? = null

    private val employeeService by lazy { EmployeeCompragaService.create() }
    private val cardService by lazy { CardCompragaService.create() }

    private val isEditable get() = mode == Mode.EDIT || mode == Mode.REGISTER

    override val title: String by lazy {
        when (mode) {
            Mode.VIEW -> getString(R.string.view_employee_data)
            Mode.EDIT -> getString(R.string.edit_employee_data)
            Mode.REGISTER -> getString(R.string.register_employee)
        }
    }

    override val navMenuItemId: Int by lazy {
        if (superior != null) R.id.navRegisterEmployee else R.id.navEmployeeData
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(isEditable)
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_employee_data, container, false)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.employee_data_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.saveButton -> {
                registerEmployee()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        savedInstanceState?.let {
            employeeDataContainer.visibility = it.getInt(STATE_EDIT_CONTROLS_VISIBILITY)
            photoUri = it.getParcelable(STATE_PHOTO_URI)
        }

        photoUri?.let(employeeImage::loadImage)

        cameraFab.visibility = if (mode == Mode.EDIT || mode == Mode.REGISTER) {
            cameraFab.setOnClickListener { takePhoto() }
            View.VISIBLE
        } else View.GONE

        pictureTextView.text = if (mode == Mode.VIEW) "No picture available" else "Take a picture"

        editControlsContainer.visibility =
                if (mode == Mode.EDIT || mode == Mode.REGISTER) View.VISIBLE
                else View.GONE

        textViewsContainer.visibility = if (mode == Mode.VIEW) View.VISIBLE else View.GONE
    }

    private fun takePhoto() {
        if (checkCameraHardware()) {
            if (checkCameraPermission()) {
                startCameraActivity()
            } else {
                askForCameraPermission()
            }
        }
    }

    private fun checkCameraHardware(): Boolean =
            context.packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)

    private fun checkCameraPermission() =
            ContextCompat.checkSelfPermission(activity, Manifest.permission.CAMERA) ==
                    PackageManager.PERMISSION_GRANTED

    private fun askForCameraPermission() {
        requestPermissions(arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA_PERMISSION)
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {

        if (requestCode == REQUEST_CAMERA_PERMISSION && grantResults.isNotEmpty() &&
                grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            startCameraActivity()
            return
        }
    }

    private fun startCameraActivity() {
        tempPhotoUri = createPhotoUri()
        val intent = tempPhotoUri?.let { createTakePhotoIntent(it) }
        intent?.let { startActivityForResult(it, REQUEST_TAKE_PHOTO) }
    }

    private fun createPhotoUri(): Uri? {
        val file = try {
            createImageFile()
        } catch (ex: IOException) {
            error("Picture could not be taken", ex)
            null
        }

        return file?.let {
            FileProvider.getUriForFile(context, FILE_PROVIDER_AUTHORITY, it)
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "PNG_${timeStamp}_"
        val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(imageFileName, ".png", storageDir)
    }

    private fun createTakePhotoIntent(photoUri: Uri): Intent? {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                .putExtra(MediaStore.EXTRA_OUTPUT, photoUri)
        return intent.resolveActivity(context.packageManager)?.let { intent }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            photoUri = tempPhotoUri
            photoUri?.let(employeeImage::loadImage)
        }
    }

    override fun onResume() {
        super.onResume()
        pictureTextView.visibility = if (photoUri == null) View.VISIBLE else View.GONE
        employee?.let {
            loadEmployeeData(it.cardnumber)
        }
    }

    // TODO Check if fields are filled
    private fun createEmployee(base64Image: String, response: CardResponse) = Employee().apply {
        this.cardnumber = response.number
        firstname = firstnameEdit.text.toString()
        lastname = lastnameEdit.text.toString()
        company = superior?.company
        email = response.emailAddress
        superioremail = superior?.email
        image = base64Image
    }

    private fun createCardRequest(imageHash: String) = CardCreateRequest().apply {
        emailAddress = emailEdit.text.toString()
        limitTransaction = transactionLimitEdit.text.toString().toIntOrNull() ?: 0
        limitFix = monthLimitEdit.text.toString().toIntOrNull() ?: 0
        limitCurrent = limitFix
        this.imageHash = imageHash
    }

    private fun registerEmployee() {
        activity.hideKeyboard()
        val photoUri = this.photoUri

        if (photoUri == null) {
            toast(R.string.picture_needed)
            return
        }

        if (emailEdit.text.toString().isBlank()) {
            toast(R.string.email_required)
            return
        }

        navigationActivity?.setLoading(true)
        subscription?.dispose()
        subscription = context.getBitmapObservable(photoUri, IMAGE_SIZE, IMAGE_SIZE)
                .flatMap { bitmap ->
                    val base64Image = bitmap.toBase64()
                    val imageHash = base64Image.hash()!!.toBase64()
                    cardService.createCard(createCardRequest(imageHash)).map { cardResponse ->
                        base64Image to cardResponse
                    }
                }
                .flatMap { (base64Image, cardResponse) ->
                    employeeService.addEmployee(createEmployee(base64Image, cardResponse))
                }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = {
                            alert("Card number: ${it.cardnumber}", "Employee registered") {
                                okButton {}
                            }.show()
                        },
                        onError = {
                            error("Employee could not be created", it)
                            longToast(it.localizedMessage);
                            navigationActivity?.setLoading(false)
                        },
                        onComplete = {
                            navigationActivity?.setLoading(false)
                        }
                )
    }

    private fun loadEmployeeData(cardNumber: Long) {
        navigationActivity?.setLoading(true)
        subscription?.dispose()
        subscription = employeeService.getEmployeeByCardnumber(cardNumber)
                .flatMap { employee ->
                    cardService.getCardByNumber(employee.cardnumber).map { card ->
                        employee to card
                    }
                }
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onNext = { (employee, card) ->
                            displayEmployeeData(employee, card)
                            employeeImage.imageBitmap = employee.image?.let {
                                bitmapFromBase64(it)
                            }
                        },
                        onError = {
                            error("Could not load employee")
                            longToast(it.localizedMessage)
                            navigationActivity?.setLoading(false)
                        },
                        onComplete = {
                            navigationActivity?.setLoading(false)
                        }
                )
    }

    private fun displayEmployeeData(employee: Employee, card: CardResponse) {
        emailView.text = employee.email
        firstnameView.text = employee.firstname
        lastnameView.text = employee.lastname
        transactionLimitView.text = card.limitTransaction.toString()
        monthLimitView.text = card.limitFix.toString()
    }

    override fun onPause() {
        super.onPause()
        subscription?.dispose()
        navigationActivity?.setLoading(false)
        activity.hideKeyboard()
    }

    override fun onDetach() {
        super.onDetach()
        navigationActivity = null
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        navigationActivity = context as? NavigationMainActivity ?: throw RuntimeException(
                "$context must implement ${NavigationMainActivity::class}"
        )
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.apply {
            putInt(STATE_EDIT_CONTROLS_VISIBILITY, employeeDataContainer.visibility)
            putParcelable(STATE_PHOTO_URI, photoUri)
        }
    }

    enum class Mode {
        VIEW,
        EDIT,
        REGISTER
    }

    companion object {
        private const val ARGUMENT_MODE = "ARGUMENT_MODE"
        private const val ARGUMENT_SUPERIOR = "ARGUMENT_SUPERIOR"
        private const val ARGUMENT_EMPLOYEE = "ARGUMENT_EMPLOYEE"

        private const val STATE_EDIT_CONTROLS_VISIBILITY = "STATE_EDIT_CONTROLS_VISIBILITY"
        private const val STATE_PHOTO_URI = "STATE_PHOTO_URI"

        private const val REQUEST_CAMERA_PERMISSION = 1
        private const val REQUEST_TAKE_PHOTO = 2

        private const val FILE_PROVIDER_AUTHORITY = "de.lmu.compragaapp.fileprovider"

        private const val IMAGE_SIZE = 640

        fun newInstance(mode: Mode) = EmployeeDataFragment().apply {
            arguments = Bundle().apply {
                putString(ARGUMENT_MODE, mode.name)
            }
        }

        fun newInstance(mode: Mode, superior: Superior) = newInstance(mode).apply {
            arguments.putSerializable(ARGUMENT_SUPERIOR, superior)
        }

        fun newInstance(employee: Employee) = newInstance(Mode.VIEW).apply {
            arguments.putSerializable(ARGUMENT_EMPLOYEE, employee)
        }
    }
}
