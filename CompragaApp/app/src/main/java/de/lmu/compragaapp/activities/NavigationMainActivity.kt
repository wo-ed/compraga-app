package de.lmu.compragaapp.activities

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import de.lmu.compragaapp.R
import de.lmu.compragaapp.fragments.MainContent
import kotlinx.android.synthetic.main.activity_navigation_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.loading_overlay.*

abstract class NavigationMainActivity(private val navigationViewMenuId: Int)
    : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation_main)

        setSupportActionBar(toolbar)

        viewStubPageContent.apply {
            layoutResource = R.layout.content_main
            inflate()
        }

        val toggle = ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)
        navView.inflateMenu(navigationViewMenuId)

        supportFragmentManager.addOnBackStackChangedListener {

            // Needed to set the title & navigation menu item when the user
            // goes back to the previous fragment
            (supportFragmentManager.findFragmentById(R.id.contentMain) as? MainContent)?.let {
                setMainContent(it)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        // Needed to set the navigation menu item when the screen is rotated
        (supportFragmentManager.findFragmentById(R.id.contentMain) as? MainContent)?.let {
            setMainContent(it)
        }
    }

    fun setMainContent(mainContent: MainContent) {
        resetMainContent()
        mainContent.title?.let { title = it }
        mainContent.navMenuItemId?.let {
            navView?.menu?.findItem(it)?.isChecked = true
        }
    }

    private fun resetMainContent() {
        title = getString(R.string.app_name)
        uncheckAllNavMenuItems()
    }

    private fun uncheckAllNavMenuItems() {
        (0 until (navView?.menu?.size() ?: 0))
                .mapNotNull { navView?.menu?.getItem(it) }
                .forEach { it.isChecked = false }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        clearBackStack()
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    fun replaceMainContentFragment(fragment: Fragment, addToBackStack: Boolean = false) {
        val transaction = supportFragmentManager.beginTransaction()
                .replace(R.id.contentMain, fragment)

        if (addToBackStack) {
            transaction.addToBackStack(null)
        }
        transaction.commit()
    }

    abstract fun createDefaultFragment(): Fragment

    fun returnToMainPage() {
        clearBackStack()
        replaceMainContentFragment(createDefaultFragment())
    }

    private fun clearBackStack() {
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }

    fun setLoading(isLoading: Boolean) {
        loadingOverlay.visibility = if (isLoading) View.VISIBLE else View.GONE
    }

    override fun onAttachFragment(fragment: Fragment) {
        super.onAttachFragment(fragment)
        if (fragment is MainContent) {
            setMainContent(fragment)
        }
    }
}
