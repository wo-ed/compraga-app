package de.lmu.compragaapp.restservices

import de.lmu.compragaapp.buildCompragaRetrofit
import de.lmu.compragaapp.entities.Request.CardCreateRequest
import de.lmu.compragaapp.entities.Request.CardUpdateRequest
import de.lmu.compragaapp.entities.Response.CardImageResponse
import de.lmu.compragaapp.entities.Response.CardResponse
import io.reactivex.Observable
import retrofit2.http.*

interface CardCompragaService {

    @PATCH("card")
    fun updateCard(@Body input: CardUpdateRequest)

    @POST("card")
    fun createCard(@Body input: CardCreateRequest): Observable<CardResponse>

    @GET("card/email={email}/")
    fun getCardByEmailAddress(@Path("email") email: String): Observable<CardResponse>

    @GET("card/number={number}")
    fun getCardByNumber(@Path("number") number: Long): Observable<CardResponse>

    companion object {
        fun create(): CardCompragaService =
                buildCompragaRetrofit().create(CardCompragaService::class.java)
    }
}