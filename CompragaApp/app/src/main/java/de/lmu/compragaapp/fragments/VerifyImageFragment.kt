package de.lmu.compragaapp.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.lmu.compragaapp.R
import de.lmu.compragaapp.entities.Employee
import de.lmu.compragaapp.utils.bitmapFromBase64
import kotlinx.android.synthetic.main.fragment_verify_image.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.imageBitmap

class VerifyImageFragment : Fragment(), AnkoLogger {

    private val employee by lazy { arguments.getSerializable(ARGUMENT_EMPLOYEE) as Employee }
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_verify_image, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        employeeImage.imageBitmap = employee.image?.let(::bitmapFromBase64)

        acceptButton.setOnClickListener { listener?.onImageVerificationAccepted(employee) }
        cancelButton.setOnClickListener { listener?.onImageVerificationCancelled(employee) }
    }

    interface OnFragmentInteractionListener {
        fun onImageVerificationAccepted(employee: Employee)
        fun onImageVerificationCancelled(employee: Employee)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as? OnFragmentInteractionListener ?: throw RuntimeException(
                "$context must implement ${OnFragmentInteractionListener::class}"
        )
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    companion object {
        private const val ARGUMENT_EMPLOYEE = "ARGUMENT_EMPLOYEE"

        fun newInstance(employee: Employee) = VerifyImageFragment().apply {
            arguments = Bundle().apply {
                putSerializable(ARGUMENT_EMPLOYEE, employee)
            }
        }
    }
}
