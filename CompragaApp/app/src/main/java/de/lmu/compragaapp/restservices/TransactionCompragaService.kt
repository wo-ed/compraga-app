package de.lmu.compragaapp.restservices

import de.lmu.compragaapp.buildCompragaRetrofit
import de.lmu.compragaapp.entities.Request.TransactionValidateRequest
import de.lmu.compragaapp.entities.Response.TransactionValidateResponse
import de.lmu.compragaapp.entities.Transaction
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface TransactionCompragaService {

    @GET("transaction")
    fun getTransactionsByCardNumber(): Observable<List<Transaction>>

    @GET("transaction/cardnumber={cardnumber}")
    fun getTransactionsByCardNumber(@Path("cardnumber") cardnumber: Long): Observable<List<Transaction>>

    @GET("transaction/shopname={shopname}")
    fun getTransactionByShopName(@Path("shopname") shopName: String): Observable<List<Transaction>>

    @GET("transaction/company={company}")
    fun getTransactionByCompanyName(@Path("company") companyName: String): Observable<List<Transaction>>

    @POST("transaction/create")
    fun createTransaction(@Body transaction: Transaction): Observable<Transaction>

    @POST("transaction/validate")
    fun validateTransaction(@Body transaction: TransactionValidateRequest): Observable<TransactionValidateResponse>

    companion object {
        fun create(): TransactionCompragaService =
                buildCompragaRetrofit().create(TransactionCompragaService::class.java)
    }
}