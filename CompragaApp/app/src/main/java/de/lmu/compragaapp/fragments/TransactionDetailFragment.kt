package de.lmu.compragaapp.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import de.lmu.compragaapp.R
import de.lmu.compragaapp.adapters.ArticlesAdapter
import de.lmu.compragaapp.entities.Transaction
import kotlinx.android.synthetic.main.fragment_transaction_detail.*
import java.text.DateFormat
import java.text.SimpleDateFormat

class TransactionDetailFragment : Fragment(), MainContent {
    override val title: String by lazy { getString(R.string.transaction_detail) }
    override val navMenuItemId: Int? = null

    private val transaction: Transaction by lazy {
        arguments.getSerializable(ARGUMENT_TRANSACTION) as Transaction
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_transaction_detail, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        totalAmountView.text = "${transaction.payload} €"
        shopView.text = transaction.shopname
        companyView.text = transaction.company
        dateView.text = transaction.transactiondate?.let(dateFormat::format)

        articlesView.apply {
            adapter = ArticlesAdapter(context, transaction.articles, null)
            val orientation = LinearLayoutManager.VERTICAL
            layoutManager = LinearLayoutManager(context, orientation, false)
            addItemDecoration(DividerItemDecoration(context, orientation))
        }
    }

    companion object {
        private const val ARGUMENT_TRANSACTION = "ARGUMENT_TRANSACTION"

        val dateFormat: DateFormat = SimpleDateFormat.getDateInstance()

        fun newInstance(transaction: Transaction) = TransactionDetailFragment().apply {
            arguments = Bundle().apply {
                putSerializable(ARGUMENT_TRANSACTION, transaction)
            }
        }
    }
}
