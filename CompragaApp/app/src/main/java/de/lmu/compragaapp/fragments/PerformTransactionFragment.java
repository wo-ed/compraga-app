package de.lmu.compragaapp.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

import de.lmu.compragaapp.R;
import de.lmu.compragaapp.activities.NavigationMainActivity;
import de.lmu.compragaapp.adapters.ArticlesAdapter;
import de.lmu.compragaapp.entities.Article;
import de.lmu.compragaapp.entities.Employee;
import de.lmu.compragaapp.entities.Shop;
import de.lmu.compragaapp.entities.Transaction;


/**
 * A simple {@link Fragment} subclass.
 */
public class PerformTransactionFragment extends Fragment implements MainContent, ArticlesAdapter.ClickListener{

    public static final String ARGUMENT_EMPLOYEE = "ARGUMENT_EMPLOYEE";
    public static final String ARGUMENT_SHOP = "ARGUMENT_SHOP";

    RecyclerView rv;
    FloatingActionButton fab;
    private AlertDialog dialog;
    // List to store articles.
    ArrayList<Article> articleList = new ArrayList<>();
    Transaction transaction;
    private int itemClickPosition;
    private ArticlesAdapter.ClickListener rvClickListener;

    private final int contextMenuDeleteId = 0;
    private final int contextMenuEditId = 1;

    private NavigationMainActivity navigationActivity;

    private Employee employee;
    private Shop shop;

    public PerformTransactionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        employee = (Employee)getArguments().getSerializable(ARGUMENT_EMPLOYEE);
        shop = (Shop)getArguments().getSerializable(ARGUMENT_SHOP);

        if( savedInstanceState != null)
        {
            articleList = (ArrayList<Article>) savedInstanceState.getSerializable("articleList");
            transaction = (Transaction) savedInstanceState.getSerializable("transaction");
        }else
        {
            transaction = new Transaction();
            transaction.setCompany(employee.getCompany());
            transaction.setCardnumber(employee.getCardnumber());
            transaction.setShopname(shop.getName());
            transaction.setArticles(articleList);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_perform_transaction, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initRecyclerView(view);
        initFab(view);
        initTransactionButton();
        updateSumText();
        registerForContextMenu(rv);
    }

    private void updateSumText()
    {
        TextView sumText = (TextView)getView().findViewById(R.id.totalSumText);
        if(transaction.getArticles().size() != 0 )
        {
            sumText.setText("Total: " +Double.toString(transaction.getPayload()));
        }
    }

    // Helper function
    private void initTransactionButton()
    {
        getView().findViewById(R.id.taButton).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(transaction.getArticles().size() != 0)
                {
                    navigationActivity.replaceMainContentFragment(InvoiceFragment.Companion.newInstance(transaction, employee, shop), true);
                }else
                {
                    String message = "Please add an article";
                    Toast toast = Toast.makeText(getContext(), message, Toast.LENGTH_SHORT);
                    toast.show();
                }

            }
        });
    }

    // Helper function to initialize the floating action button
    private void initFab(View view)
    {
        // Floating Action Button
        fab = (FloatingActionButton)view.findViewById(R.id.addArticleButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                LayoutInflater inflater = getActivity().getLayoutInflater();

                builder.setTitle("Add article");
                final View layout = inflater.inflate(R.layout.fragment_add_article, null);
                builder.setView(layout);
                builder.create();

                //final AlertDialog dialog = builder.show();
                dialog = builder.show();

                Button addArticleButton = (Button)layout.findViewById(R.id.addArticleDialogButton);
                Button cancelDialogButton = (Button)layout.findViewById(R.id.cancelDialogButton);
                Button nextArticleButton = (Button)layout.findViewById(R.id.nextArticleDialogButton);

                // Listener for the addArticleButton in the Dialog
                addArticleButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View vi)
                    {

                        EditText articleText = (EditText)layout.findViewById(R.id.articleId);
                        EditText priceText = (EditText)layout.findViewById(R.id.priceId);
                        EditText amountText = (EditText)layout.findViewById(R.id.amountId);

                        if( articleText.getText().length() != 0 && priceText.getText().length() != 0 && amountText.getText().length() != 0)
                        {
                            addArticle(articleText, priceText, amountText);
                        }

                        dialog.dismiss();
                    }
                });

                nextArticleButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {

                        EditText articleText = (EditText)layout.findViewById(R.id.articleId);
                        EditText priceText = (EditText)layout.findViewById(R.id.priceId);
                        EditText amountText = (EditText)layout.findViewById(R.id.amountId);

                        if( articleText.getText().length() != 0 && priceText.getText().length() != 0 && amountText.getText().length() != 0)
                        {
                            addArticle(articleText, priceText, amountText);
                        }

                        articleText.setText("");
                        priceText.setText("");
                        amountText.setText("");
                    }
                });

                // Listener for the cancelDialogButton in the Dialog
                cancelDialogButton.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View vi)
                    {
                        dialog.dismiss();
                    }
                });
            }
        });
    }

    private void addArticle(EditText articleText, EditText priceText, EditText amountText)
    {
        String name = articleText.getText().toString();
        Double price = Double.parseDouble(priceText.getText().toString());
        int amount = Integer.parseInt(amountText.getText().toString());
        articleList.add(0, new Article(name, amount, price));
        rv.getAdapter().notifyItemInserted(0);
        updateSumText();
    }

    // Helper function to initialize the recyclerview.
    private void initRecyclerView(View view)
    {

        rv = (RecyclerView)view.findViewById(R.id.performTransactionRecyclerView);
        rv.setAdapter(new ArticlesAdapter(getContext(), articleList, this));
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        rv.setLayoutManager(layoutManager);
        rv.addItemDecoration(new DividerItemDecoration(getContext(), layoutManager.getOrientation()));
    }

    private void deleteArticle()
    {
       //Toast.makeText(getContext(), "Position: "+itemClickPosition, Toast.LENGTH_SHORT).show();
        transaction.getArticles().remove(itemClickPosition);
        rv.getAdapter().notifyDataSetChanged();
        updateSumText();
    }

    private void editArticle()
    {
        final EditText name = (EditText)dialog.findViewById(R.id.articleId);
        final EditText price= (EditText)dialog.findViewById(R.id.priceId);
        final EditText amount= (EditText)dialog.findViewById(R.id.amountId);

        name.setText(transaction.getArticles().get(itemClickPosition).getName());
        price.setText(Double.toString(transaction.getArticles().get(itemClickPosition).getPrice()));
        amount.setText(Integer.toString(transaction.getArticles().get(itemClickPosition).getAmount()));

        dialog.show();

        Button addArticleButton = (Button)dialog.findViewById(R.id.addArticleDialogButton);
        Button nextArticleButton = (Button)dialog.findViewById(R.id.nextArticleDialogButton);

        if( nextArticleButton != null )
        {
            ViewGroup viewGroup = (ViewGroup)nextArticleButton.getParent();
            viewGroup.removeView(nextArticleButton);
        }

        addArticleButton.setText("Confirm");

        addArticleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Remove old transaction and create new one
                transaction.getArticles().remove(itemClickPosition);
                transaction.getArticles().add(itemClickPosition, new Article(
                        name.getText().toString(),
                                Integer.parseInt(amount.getText().toString()),
                        Double.parseDouble(price.getText().toString())));
                rv.getAdapter().notifyDataSetChanged();
                updateSumText();
                dialog.dismiss();
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        //super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, contextMenuDeleteId, 0, "Delete");
        menu.add(0, contextMenuEditId, 0, "Edit");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id)
        {
            case contextMenuDeleteId: deleteArticle();
                break;
            case contextMenuEditId: editArticle();
                break;
        }

        return true;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        navigationActivity = null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if(!(context instanceof NavigationMainActivity)) {
            throw new RuntimeException(context + " must implement " + NavigationMainActivity.class);
        }

        navigationActivity = (NavigationMainActivity) context;
    }

    @Override
    public String getTitle()
    {
        return getString(R.string.perform_transaction);
    }

    @Nullable
    @Override
    public Integer getNavMenuItemId()
    {
        return null;
    }

    public static PerformTransactionFragment newInstance(Shop shop, Employee employee)
    {
        PerformTransactionFragment fragment = new PerformTransactionFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(ARGUMENT_SHOP, shop);
        bundle.putSerializable(ARGUMENT_EMPLOYEE, employee);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable("articleList", articleList);
        outState.putSerializable("transaction", transaction);
    }

    @Override
    public void onItemClick(ArticlesAdapter.ViewHolder holder) {
        //Toast.makeText(getContext(), "Position: "+holder.getAdapterPosition(), Toast.LENGTH_SHORT).show();
        itemClickPosition = holder.getAdapterPosition();
    }


    @NotNull
    @Override
    public void onItemLongClick(ArticlesAdapter.ViewHolder holder) {
        itemClickPosition = holder.getAdapterPosition();
    }
}
