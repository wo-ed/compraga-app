package de.lmu.compragaapp.activities

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import de.lmu.compragaapp.R
import de.lmu.compragaapp.Role
import de.lmu.compragaapp.entities.Employee
import de.lmu.compragaapp.fragments.CredentialsFragment
import kotlinx.android.synthetic.main.activity_login.*
import org.jetbrains.anko.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        superiorButton.setOnClickListener { showLogin(Role.SUPERIOR) }
        employeeButton.setOnClickListener { showLogin(Role.EMPLOYEE) }
        shopButton.setOnClickListener { showLogin(Role.SHOP) }

        verifyCardButton.setOnClickListener {
            startActivityForResult(intentFor<VerifyCardActivity>(), REQUEST_VERIFY_CARD)
        }
    }

    fun showLogin(role: Role) = replaceMainContentFragment(CredentialsFragment.newInstance(role),
                    addToBackStack = true)

    fun replaceMainContentFragment(fragment: Fragment, addToBackStack: Boolean = false) {
        val transaction = supportFragmentManager.beginTransaction()
                .replace(R.id.contentMain, fragment)

        if (addToBackStack) {
            transaction.addToBackStack(null)
        }
        transaction.commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == LoginActivity.REQUEST_VERIFY_CARD && resultCode == Activity.RESULT_OK) {
            val employee = data?.getSerializableExtra(VerifyCardActivity.RESULT_EMPLOYEE) as Employee

            alert("Do you want to login to perform a transaction?", "Login required") {
                yesButton {
                    replaceMainContentFragment(CredentialsFragment.newInstance(employee),
                            addToBackStack = false)
                }
                noButton {}
            }.show()
        }
    }

    companion object {
        const val REQUEST_VERIFY_CARD = 1

        fun start(ctx: Context) {
            ctx.startActivity(ctx.intentFor<LoginActivity>())
        }
    }
}
