package de.lmu.compragaapp.entities

import java.io.Serializable
import java.sql.Date

class Transaction: Serializable {
    var payload = 0.0
        get() = articles.map { it.price * it.amount }.fold(0.0) { acc, i -> acc + i }

    var cardnumber: Long = 0
    var shopname: String? = null
    var company: String? = null
    var transactiondate: Date? = null
    var articles: List<Article> = listOf()
}
