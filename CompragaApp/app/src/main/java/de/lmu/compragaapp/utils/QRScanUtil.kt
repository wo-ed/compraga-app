package de.lmu.compragaapp.utils

import android.app.Activity
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.CaptureActivity

fun Activity.startScanActivity() = IntentIntegrator(this)
        .setOrientationLocked(false)
        .setBeepEnabled(true)
        .initiateScan()