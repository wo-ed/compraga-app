package de.lmu.compragaapp.utils

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.PorterDuff
import android.graphics.Rect
import android.net.Uri
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.util.Base64
import android.view.Menu
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.target.Target
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import org.jetbrains.anko.inputMethodManager
import org.jetbrains.anko.itemsSequence
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast
import java.io.ByteArrayOutputStream

fun Activity.threadToast(message: String) {
    runOnUiThread {
        toast(message)
    }
}

fun Activity.threadToast(message: Int) {
    runOnUiThread {
        toast(message)
    }
}

fun Activity.threadLongToast(message: String) {
    runOnUiThread {
        longToast(message)
    }
}

fun Activity.threadLongToast(message: Int) {
    runOnUiThread {
        longToast(message)
    }
}

fun Context.getCompatColor(color: Int): Int {
    return ContextCompat.getColor(this, color)
}

fun android.support.v4.app.Fragment.getCompatColor(color: Int): Int {
    return context.getCompatColor(color)
}

fun Menu.setIconsTint(color: Int) {
    itemsSequence()
            .mapNotNull { it.icon }
            .forEach { it.mutate().setColorFilter(color, PorterDuff.Mode.SRC_ATOP) }
}

fun Activity.hideKeyboard() = this.currentFocus?.let {
    this.inputMethodManager.hideSoftInputFromWindow(it.windowToken, 0)
}

fun ImageView.loadImage(url: String): Target<GlideDrawable> =
        Glide.with(context).load(url).into(this)

fun ImageView.loadImage(uri: Uri): Target<GlideDrawable> =
        Glide.with(context).load(uri).into(this)

fun Bitmap.toBase64(quality: Int = 100) =
        Base64.encodeToString(this.toByteArray(quality), Base64.DEFAULT)

fun Bitmap.toByteArray(quality: Int = 100): ByteArray = ByteArrayOutputStream().use { stream ->
    this.compress(Bitmap.CompressFormat.PNG, quality, stream)
    return stream.toByteArray()
}

fun bitmapFromBase64(encoded: String): Bitmap? {
    val decoded = Base64.decode(encoded, Base64.DEFAULT)
    return BitmapFactory.decodeByteArray(decoded, 0, decoded.size)
}

fun Context.getBitmap(uri: Uri) = MediaStore.Images.Media.getBitmap(contentResolver, uri)

fun Context.getBitmap(uri: Uri, reqWidth: Int, reqHeight: Int): Bitmap {
    val outPadding = Rect()
    val options = BitmapFactory.Options()

    contentResolver.openInputStream(uri).use { stream ->

        // First decode with inJustDecodeBounds=true to check dimensions
        options.inJustDecodeBounds = true

        BitmapFactory.decodeStream(stream, outPadding, options)
    }

    return contentResolver.openInputStream(uri).use { stream ->
        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight)

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false

        BitmapFactory.decodeStream(stream, outPadding, options)
    }
}

fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
    // Raw height and width of image
    val height = options.outHeight
    val width = options.outWidth
    var inSampleSize = 1

    if (height > reqHeight || width > reqWidth) {
        val halfHeight = height / 2
        val halfWidth = width / 2

        // Calculate the largest inSampleSize value that is a power of 2 and keeps both
        // height and width larger than the requested height and width.
        while (halfHeight / inSampleSize >= reqHeight && halfWidth / inSampleSize >= reqWidth) {
            inSampleSize *= 2
        }
    }

    return inSampleSize
}

fun Context.getBitmapObservable(uri: Uri): Observable<Bitmap> = Observable.fromCallable {
    getBitmap(uri)
}

fun Context.getBitmapObservable(uri: Uri, reqWith: Int, reqHeight: Int): Observable<Bitmap> =
        Observable.fromCallable { getBitmap(uri, reqWith, reqHeight) }

fun <T : Any, U : Any> Observable<T>.zipTuple(other: Observable<U>) {
    this.zipWith(other, BiFunction<T, U, Pair<T, U>> { t, u ->
        t to u
    })
}