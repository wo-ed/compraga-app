package de.lmu.compragaapp.utils

import android.util.Base64
import java.security.MessageDigest

fun String.hash(): ByteArray? {
    val md = MessageDigest.getInstance("SHA-1")
    md.update(this.toByteArray())
    return md.digest()
}

fun ByteArray.toBase64(): String = Base64.encodeToString(this, Base64.DEFAULT)

