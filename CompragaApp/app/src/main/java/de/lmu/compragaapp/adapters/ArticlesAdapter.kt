package de.lmu.compragaapp.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import de.lmu.compragaapp.R
import de.lmu.compragaapp.entities.Article
import org.jetbrains.anko.find
import org.jetbrains.anko.layoutInflater

class ArticlesAdapter(private val context: Context,
                      private val articles: List<Article>,
                    private val clickListener: ClickListener?)
    : RecyclerView.Adapter<ArticlesAdapter.ViewHolder>() {

    override fun getItemCount() = articles.size

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int) =
        ViewHolder(context.layoutInflater.inflate(R.layout.list_item_article, parent, false))

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bind(articles[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val nameView = itemView.find<TextView>(R.id.articleNameView)
        private val priceView = itemView.find<TextView>(R.id.amountView)
        lateinit var article: Article

        init
        {
            itemView.isLongClickable = true
            itemView.setOnClickListener { clickListener?.onItemClick(this) }
            itemView.setOnLongClickListener{
                clickListener?.onItemLongClick(this)
                false
            }
        }

        fun bind(article: Article) {
            this.article = article
            nameView.text = "${article.name} (${article.amount})"
            priceView.text = "${article.price} €"
        }
    }

    interface ClickListener
    {
        fun onItemClick(holder: ViewHolder)
        fun onItemLongClick(holder: ViewHolder)
    }
}