package de.lmu.compragaapp.entities.Request

class CardCreateRequest {
    var emailAddress: String? = null
    var limitTransaction: Int = 0
    var limitCurrent: Int = 0
    var limitFix: Int = 0
    var imageHash: String? = null
}
