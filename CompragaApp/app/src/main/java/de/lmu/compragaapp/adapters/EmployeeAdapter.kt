package de.lmu.compragaapp.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import java.util.ArrayList

import de.lmu.compragaapp.R
import de.lmu.compragaapp.entities.Employee
import org.jetbrains.anko.find

class EmployeeAdapter(private val employees: ArrayList<Employee>,
                      val clickListener: ClickListener?)
    : RecyclerView.Adapter<EmployeeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.recycler_view_text, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: EmployeeAdapter.ViewHolder, position: Int) {
        val employee = employees[position]
        holder.textView.text = "${employee.firstname} ${employee.lastname}"
    }

    override fun getItemCount() = employees.size

    interface ClickListener {
        fun onEmployeeClick(holder: ViewHolder)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val textView: TextView = itemView.find<TextView>(R.id.recycler_view_text)

        init {
            itemView.setOnClickListener { clickListener?.onEmployeeClick(this) }
        }
    }
}
