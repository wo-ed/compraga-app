package api.repositories;

import api.entities.Shop;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface ShopRepository extends CrudRepository<Shop, Long>{

    Collection<Shop> findAll();
    Shop findByEmail(String email);
    Collection<Shop> findByName(String name);
}
