package api.repositories;

import api.entities.Employee;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface EmployeeRepository extends CrudRepository<Employee, Long>{

    Collection<Employee> findAll();
    Employee findByEmail(String email);
    Employee findByCardnumber(long cardnumber);

    Collection<Employee> findByCompany(String company);
    Collection<Employee> findByFirstname(String firstname);
    Collection<Employee> findByLastname(String lastname);
    Collection<Employee> findBySuperioremail(String superioremail);
    Collection<Employee> findByFirstnameAndLastname(String firstname, String lastname);
    Collection<Employee> findByCompanyAndFirstname(String company, String firstname);
    Collection<Employee> findByCompanyAndLastname(String company, String lastname);
    Collection<Employee> findByCompanyAndFirstnameAndLastname(String company, String firstname, String lastname);
    Collection<Employee> findByCompanyAndSuperioremail(String company, String superioremail);

}