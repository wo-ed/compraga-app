package api.repositories;

import api.entities.Superior;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface SuperiorRepository extends CrudRepository<Superior, Long>{

    Collection<Superior> findAll();
    Superior findByEmail(String email);
    Collection<Superior> findByFirstname(String firstname);
    Collection<Superior> findByLastname(String lastname);
}
