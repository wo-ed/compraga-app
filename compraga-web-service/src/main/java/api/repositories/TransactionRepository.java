package api.repositories;

import api.entities.Transaction;
import org.springframework.data.repository.CrudRepository;

import java.sql.Date;
import java.util.Collection;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    Collection<Transaction> findAll();
    Collection<Transaction> findByCardnumber(long cardnumber);
    Collection<Transaction> findByShopname(String shopname);
    Collection<Transaction> findByCompany(String company);
    Collection<Transaction> findByCompanyAndCardnumber(String company, long cardnumber);
}
