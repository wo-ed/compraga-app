package api.dataloader;

import api.entities.Employee;
import api.entities.Shop;
import api.entities.Superior;
import api.entities.Transaction;
import api.repositories.EmployeeRepository;
import api.repositories.ShopRepository;
import api.repositories.SuperiorRepository;
import api.repositories.TransactionRepository;
import api.services.SmartContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Int256;
import org.web3j.abi.datatypes.generated.Uint256;

import java.util.Random;
import java.util.UUID;

import java.sql.Date;

@Component
public class DataLoader implements ApplicationRunner{

    private EmployeeRepository employeeRepo;
    private ShopRepository shopRepo;
    private SuperiorRepository superiorRepo;
    private TransactionRepository transactionRepo;
    @Autowired
    private SmartContractService smartContractService;

    @Autowired
    public DataLoader(EmployeeRepository employeeRepo, ShopRepository shopRepo, SuperiorRepository superiorRepo, TransactionRepository transactionRepo, SmartContractService smartContractService) {
        this.employeeRepo = employeeRepo;
        this.shopRepo = shopRepo;
        this.superiorRepo = superiorRepo;
        this.transactionRepo = transactionRepo;
        this.smartContractService = smartContractService;
    }

    public void run(ApplicationArguments args) throws Exception{

        Random random1 = new Random();
        Random random2 = new Random();
        Random random3 = new Random();
        Random random4 = new Random();
        Random random5 = new Random();

        int number1 = Integer.parseInt("11111" + (random1.nextInt(900)+100));
        int number2 = Integer.parseInt("11111" + (random2.nextInt(900)+100));
        int number3 = Integer.parseInt("11111" + (random3.nextInt(900)+100));
        int number4 = Integer.parseInt("11111" + (random4.nextInt(900)+100));
        int number5 = Integer.parseInt("11111" + (random5.nextInt(900)+100));

        smartContractService.getContract().createCard(
                new Uint256(number1),
                new Utf8String( "philipp@compraga.de" ),
                new Int256(500),
                new Int256(2000),
                new Int256(2000),
                new Utf8String("testHash")
                ).get();

        System.out.println();
        System.out.print("new cardnumber for philipp@compraga.de created ===> "+ number1);
        System.out.println();

        employeeRepo.save(new Employee("Philipp", "Janssen", "philipp@compraga.de", "Compraga AG", "uwewalter@compraga.de", number1, "imageString1"));

         smartContractService.getContract().createCard(
           new Uint256(number2),
           new Utf8String( "roman@compraga.de" ),
           new Int256(500),
           new Int256(2000),
           new Int256(2000),
           new Utf8String("testHash")
           ).get();
           employeeRepo.save(new Employee("Roman", "Schader", "roman@compraga.de", "Compraga AG", "claudialinhoff-popien@compraga.de", number2, "imageString2"));

           smartContractService.getContract().createCard(
           new Uint256(number3),
           new Utf8String( "leo@compraga.de" ),
           new Int256(500),
           new Int256(2000),
           new Int256(2000),
           new Utf8String("testHash")
           ).get();
           employeeRepo.save(new Employee("Leo", "Sünkel", "leo@compraga.de", "Compraga AG", "donovanpfaff@compraga.de", number3, "imageString3"));

           smartContractService.getContract().createCard(
           new Uint256(number4),
           new Utf8String( "wolfgang@compraga.de" ),
           new Int256(500),
           new Int256(2000),
           new Int256(2000),
           new Utf8String("testHash")
           ).get();
           employeeRepo.save(new Employee("Wolfgang", "Edinger", "wolfgang@compraga.de", "Compraga AG", "andreassedlmeier@compraga.de", number4, "imageString4"));

           smartContractService.getContract().createCard(
           new Uint256(number5),
           new Utf8String( "david@compraga.de" ),
           new Int256(500),
           new Int256(2000),
           new Int256(2000),
           new Utf8String("testHash")
           ).get();
           employeeRepo.save(new Employee("David", "Heuss", "david@compraga.de", "Compraga AG", "michaelbeck@compraga.de", number5, "imageString5"));

           smartContractService.getContract().createCard(
           new Uint256(number5),
           new Utf8String( "david@compraga.de" ),
           new Int256(500),
           new Int256(2000),
           new Int256(2000),
           new Utf8String("testHash")
           ).get();

           employeeRepo.save(new Employee("David", "Heuss", "david@compraga.de", "Compraga AG", "michaelbeck@compraga.de", number5, "imageString5"));

        shopRepo.save(new Shop("Edeka", "edeka@shop.de"));
        shopRepo.save(new Shop("Rewe", "rewe@shop.de"));
        shopRepo.save(new Shop("Aldi", "aldi@shop.de"));
        shopRepo.save(new Shop("Penny", "penny@shop.de"));
        shopRepo.save(new Shop("Lidl", "lidl@shop.de"));

        superiorRepo.save(new Superior("Uwe", "Walter", "uwewalter@compraga.de", "Compraga AG"));
        superiorRepo.save(new Superior("Claudia", "Linhoff-Popien", "claudialinhoff-popien@compraga.de", "Compraga AG"));
        superiorRepo.save(new Superior("Donovan", "Pfaff", "donovanpfaff@compraga.de", "Compraga AG"));
        superiorRepo.save(new Superior("Andreas", "Sedlmeier", "andreassedlmeier@compraga.de", "Compraga AG"));
        superiorRepo.save(new Superior("Michael", "Beck", "michaelbeck@compraga.de", "Compraga AG"));

        transactionRepo.save(new Transaction(100, number1, "Edeka", "Compraga AG", Date.valueOf("2017-06-20"), "[{ \"name\": \"Tacker\", \"price\": 15.00, \"amount\": 3 }, { \"name\": \"Locher\", \"price\": 5.00, \"amount\": 11 }]"));
        transactionRepo.save(new Transaction(200, number1, "Edeka", "Compraga AG", Date.valueOf("2017-06-25"), "[{ \"name\": \"Büroleuchte\", \"price\": 100, \"amount\": 2 }]"));
        transactionRepo.save(new Transaction(15, number2, "Rewe", "Compraga AG", Date.valueOf("2017-06-25"), "[{ \"name\": \"Brötchen\", \"price\": 1.00, \"amount\": 10 }, { \"name\": \"Brezn\", \"price\": 1.00, \"amount\": 5 }]"));
        transactionRepo.save(new Transaction(10, number3, "Penny", "Compraga AG", Date.valueOf("2017-06-28"), "[{ \"name\": \"Klebeband\", \"price\": 2.00, \"amount\": 5 }]"));
        transactionRepo.save(new Transaction(50, number5, "Lidl", "Compraga AG", Date.valueOf("2017-08-02"), "[{ \"name\": \"Schaufel\", \"price\": 5.00, \"amount\": 2 }, { \"name\": \"Leberkassemmeln\", \"price\": 1.00, \"amount\": 40 }]"));

    }
}

