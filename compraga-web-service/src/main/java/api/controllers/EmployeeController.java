package api.controllers;

import api.entities.Employee;
import api.repositories.EmployeeRepository;
import jdk.nashorn.internal.runtime.URIUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriUtils;


import java.util.Collection;

import static org.apache.commons.codec.CharEncoding.UTF_8;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeRepository employeeRepo;

    //search for all employees in database
    @RequestMapping(
            method = RequestMethod.GET
    )
    public ResponseEntity<Collection<Employee>> getEmployees(){
        return new ResponseEntity<>(employeeRepo.findAll(), HttpStatus.OK);
    }

    //find employee by cardnumber
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/cardnumber={cardnumber}"
    )
    public ResponseEntity<Employee> getEmployeeByCardnumber(@PathVariable long cardnumber) {
        Employee employee = employeeRepo.findByCardnumber(cardnumber);
        if (employee != null) {
            return new ResponseEntity<>(employeeRepo.findByCardnumber(cardnumber), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }


    //search for employee with specific email address
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/email={email}"
    )
    public ResponseEntity<Employee> getEmployeeByEmail(@PathVariable String email) throws Exception{
        String decoded = UriUtils.decode(email, "UTF-8");
        Employee employee = employeeRepo.findByEmail(decoded);
        if (employee != null){
            return new ResponseEntity<>(employeeRepo.findByEmail(decoded), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //search for all employees of a specific company
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/company={company}"
    )
    public ResponseEntity<Collection<Employee>> getEmployeesByCompany(@PathVariable String company){
        Collection<Employee> employee = employeeRepo.findByCompany(company);
        if (employee != null){
            return new ResponseEntity<>(employeeRepo.findByCompany(company), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //search for all employees with a specific firstname
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/firstname={firstname}"
    )
    public ResponseEntity<Collection<Employee>> getEmployeesByFirstName(@PathVariable String firstname){
        Collection<Employee> employee = employeeRepo.findByFirstname(firstname);
        if (employee != null){
            return new ResponseEntity<>(employeeRepo.findByFirstname(firstname), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //search for all employees with a specific lastname
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/lastname={lastname}"
    )
    public ResponseEntity<Collection<Employee>> getEmployeesByLastName(@PathVariable String lastname){
        Collection<Employee> employee = employeeRepo.findByLastname(lastname);
        if (employee != null){
            return new ResponseEntity<>(employeeRepo.findByLastname(lastname), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //search for firstname and lastname
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/firstname={firstname}/lastname={lastname}"
    )
    public ResponseEntity<Collection<Employee>> getEmployeeByFirstnameAndLastname(@PathVariable String firstname, @PathVariable  String lastname){
        Collection<Employee> employee = employeeRepo.findByFirstnameAndLastname(firstname, lastname);
        if (employee != null){
            return new ResponseEntity<>(employeeRepo.findByFirstnameAndLastname(firstname, lastname), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //search for all employees with a specific superior email address
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/superioremail"
    )
    public ResponseEntity<Collection<Employee>> getEmployeeBySuperiorEmail(@PathVariable String superioremail) throws Exception {
        String decoded = UriUtils.decode(superioremail, "UTF-8");
        Collection<Employee> employee = employeeRepo.findBySuperioremail(decoded);
        if (employee != null){
            return new ResponseEntity<>(employeeRepo.findBySuperioremail(decoded), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //search for company and firstname
    @RequestMapping(
            method = RequestMethod.GET,
            value = "company={company}/firstname={firstname}"
    )
    public ResponseEntity<Collection<Employee>> getEmployeeByCompanyAndFirstname(@PathVariable String company, @PathVariable String firstname){
        Collection<Employee> employee = employeeRepo.findByCompanyAndFirstname(company, firstname);
        if (employee != null){
            return new ResponseEntity<>(employeeRepo.findByCompanyAndFirstname(company, firstname), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //serach for company and lastname
    @RequestMapping(
            method = RequestMethod.GET,
            value = "company={company}/lastname={lastname}"
    )
    public ResponseEntity<Collection<Employee>> getEmployeeByComanyAndLastname(@PathVariable String company, @PathVariable String lastname){
        Collection<Employee> employee = employeeRepo.findByCompanyAndLastname(company, lastname);
        if (employee != null){
            return new ResponseEntity<>(employeeRepo.findByCompanyAndLastname(company, lastname), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //search for company and firstname and lastname
    @RequestMapping(
            method = RequestMethod.GET,
            value = "company={company}/firstname={firstname}/lastname={lastname}"
    )
    public ResponseEntity<Collection<Employee>> getEmployeeByCompanyAndFirstnameAndLastname(@PathVariable String company, @PathVariable String firstname, @PathVariable String lastname){
        Collection<Employee> employee = employeeRepo.findByCompanyAndFirstnameAndLastname(company, firstname, lastname);
        if (employee != null){
            return new ResponseEntity<>(employeeRepo.findByCompanyAndFirstnameAndLastname(company, firstname, lastname), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //search for company and superioremailaddress
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/companyandsuperioremail"
    )
    public ResponseEntity<Collection<Employee>> getEmployeeByCompanyAndSuperiorEmail(@RequestParam String company, String superioremail){
        Collection<Employee> employee = employeeRepo.findByCompanyAndSuperioremail(company, superioremail);
        if (employee != null){
            return new ResponseEntity<>(employeeRepo.findByCompanyAndSuperioremail(company, superioremail), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //add a new emloyee to the database
    @RequestMapping(
            method = RequestMethod.POST,
            value = "/create"
    )
    public ResponseEntity<?> addEmployee(@RequestBody Employee employee){
        return new ResponseEntity<>(employeeRepo.save(employee), HttpStatus.CREATED);
    }

    //delete an employee with a specific email
    @RequestMapping(
            method = RequestMethod.DELETE,
            value = "/delete_email={email}"
    )
    public ResponseEntity<Void> deleteEmployeeByEmail(@PathVariable("email") String email) {
        Employee employee = employeeRepo.findByEmail(email);
        long tempID = employee.getId();

        if (employee == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }

        employeeRepo.delete(tempID);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}