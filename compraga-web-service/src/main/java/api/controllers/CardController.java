package api.controllers;

import api.entities.*;
import api.entities.Request.CardCreateRequest;
import api.entities.Request.CardUpdateRequest;
import api.entities.Response.CardImageResponse;
import api.entities.Response.CardResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriUtils;
import org.web3j.abi.datatypes.Bool;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Int256;
import org.web3j.abi.datatypes.generated.Uint256;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import api.services.SmartContractService;
import api.repositories.EmployeeRepository;

@RestController
@RequestMapping("/card")
public class CardController {

    @Autowired
    SmartContractService smartContractService;

    @Autowired
    private EmployeeRepository employeeRepo;

    @RequestMapping(
            method = RequestMethod.GET,
            value="/number={number}"
    )
    public ResponseEntity<CardResponse> getCardByNumber(@PathVariable("number") long number) throws Exception {
        List cardData = smartContractService.getContract().getCardByNumber(new Uint256(number))
                .get()
                .stream()
                .map(x -> x.getValue())
                .collect(Collectors.toList());

        if (number == Long.parseLong(cardData.get(0).toString())) {
            CardResponse cardResponse = new CardResponse(
                    Long.parseLong(cardData.get(0).toString()),
                    cardData.get(1).toString(),
                    Integer.parseInt(cardData.get(2).toString()),
                    Integer.parseInt(cardData.get(3).toString()),
                    Integer.parseInt(cardData.get(4).toString()),
                    cardData.get(5).toString()
            );
            return new ResponseEntity<>(cardResponse, HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(
            method = RequestMethod.GET,
            value="/email={email}"
    )
    public ResponseEntity<CardResponse> getCardByEmailAddress(@PathVariable("email") String email) throws Exception {

        String decoded = UriUtils.decode(email, "UTF-8");
        Employee employee = employeeRepo.findByEmail(decoded);

        long cardNumber = employee.getCardnumber();

        List cardData = smartContractService.getContract().getCardByNumber(new Uint256( cardNumber ))
                .get()
                .stream()
                .map(x -> x.getValue())
                .collect(Collectors.toList());

        CardResponse cardResponse = new CardResponse(
                Long.parseLong(cardData.get(0).toString()),
                cardData.get(1).toString(),
                Integer.parseInt(cardData.get(2).toString()),
                Integer.parseInt(cardData.get(3).toString()),
                Integer.parseInt(cardData.get(4).toString()),
                cardData.get(5).toString()
        );
        return new ResponseEntity<>(cardResponse, HttpStatus.OK);
    }

    @RequestMapping(
            method = RequestMethod.POST
    )
    public ResponseEntity<CardResponse> createCard(@RequestBody CardCreateRequest input ) throws Exception {
            long cardNumber = new CardNumber().number;
            System.out.println();
            System.out.println("new cardnumber created ===> "+ cardNumber);
            System.out.println();
            smartContractService.getContract().createCard(
                    new Uint256(cardNumber),
                    new Utf8String(input.getEmailAddress()),
                    new Int256(input.getLimitTransaction()),
                    new Int256(input.getLimitCurrent()),
                    new Int256(input.getLimitFix()),
                    new Utf8String(input.getImageHash())
            ).get();

            CardResponse cardResponse = new CardResponse(
                    cardNumber, input.getEmailAddress(), input.getLimitTransaction(), input.getLimitCurrent(),
                    input.getLimitFix(), input.getImageHash());
            return new ResponseEntity<CardResponse>(cardResponse, HttpStatus.CREATED);
    }

    @RequestMapping(
            method = RequestMethod.PATCH
    )
    public ResponseEntity<Void> updateCard(@RequestBody CardUpdateRequest input) throws Exception {

            long cardNumber = input.getNumber();
            Bool result = smartContractService.getContract().checkCardExistence(new Uint256(input.getNumber())).get();

            if (result.getValue()) {
                smartContractService.getContract().updateCard(
                        new Uint256(cardNumber),
                        new Utf8String(input.getEmailAddress()),
                        new Int256(input.getLimitTransaction()),
                        new Int256(input.getLimitCurrent()),
                        new Int256(input.getLimitFix()),
                        new Utf8String(input.getImageHash())
                ).get();
                return new ResponseEntity<Void>(HttpStatus.CREATED);

            } else
                return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
    }

    class CardNumber {

        int number;


        public CardNumber() {
            Random random = new Random();
            number = Integer.parseInt("11111" + (random.nextInt(900)+100));
        }
    }

}
