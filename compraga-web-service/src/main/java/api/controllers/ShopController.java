package api.controllers;

import api.entities.Employee;
import api.entities.Shop;
import api.repositories.ShopRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriUtils;

import java.util.Collection;

@RestController
@RequestMapping("shop")
public class ShopController {

    @Autowired
    private ShopRepository shopRepo;

    //search for all shops in database
    @RequestMapping(
            method = RequestMethod.GET
    )
    public ResponseEntity<Collection<Shop>> getShops(){
        return new ResponseEntity<>(shopRepo.findAll(), HttpStatus.OK);
    }

    //search for shop with specific email in database
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/email={email}"
    )
    public ResponseEntity<Shop> getShopByEmail(@PathVariable String email) throws Exception{
        String decoded = UriUtils.decode(email, "UTF-8");
        Shop shop = shopRepo.findByEmail(decoded);
        if (shop != null){
            return new ResponseEntity<>(shopRepo.findByEmail(decoded), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //search for all shops with a specific name
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/name={name}"
    )
    public ResponseEntity<Collection<Shop>> getShopsByName(@PathVariable String name){
        Collection<Shop> shop = shopRepo.findByName(name);
        if (shop != null){
            return new ResponseEntity<>(shopRepo.findByName(name), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //add a new shop to the database
    @RequestMapping(
            method = RequestMethod.POST
    )
    public ResponseEntity<?> addShop(@RequestBody Shop shop){
        return new ResponseEntity<>(shopRepo.save(shop), HttpStatus.CREATED);
    }

    //delete a shop with a specific id
    @RequestMapping(
            method = RequestMethod.DELETE,
            value = "/delete_id={id}"
    )
    public ResponseEntity<Void> deleteShopById(@PathVariable long id) {
        shopRepo.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    //delete a shop with a specific email
    @RequestMapping(
            method = RequestMethod.DELETE,
            value = "/delete_email={email}"
    )
    public ResponseEntity<Void> deleteShopByEmail(@PathVariable("email") String email) {
        Shop shop = shopRepo.findByEmail(email);
        long tempID = shop.getId();

        if (shop == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }

        shopRepo.delete(tempID);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
