package api.controllers;

import api.entities.Shop;
import api.entities.Superior;
import api.repositories.SuperiorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriUtils;

import java.util.Collection;

@RestController
@RequestMapping("superior")
public class SuperiorController {

    @Autowired
    private SuperiorRepository superiorRepo;

    //search for all superiors in database
    @RequestMapping(
            method = RequestMethod.GET
    )
    public ResponseEntity<Collection<Superior>> getSuperior(){
        return new ResponseEntity<>(superiorRepo.findAll(), HttpStatus.OK);
    }

    //search for superior with specific email in database
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/email={email}"
    )
    public ResponseEntity<Superior> getSuperiorByEmail(@PathVariable String email) throws Exception{
        String decoded = UriUtils.decode(email, "UTF-8");
        Superior superior = superiorRepo.findByEmail(decoded);
        if (superior != null){
            return new ResponseEntity<>(superiorRepo.findByEmail(decoded), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //search for all superiors with a specific firstname
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/firstname={firstname}"
    )
    public ResponseEntity<Collection<Superior>> getSuperiorByFirstName(@PathVariable String firstname){
        Collection<Superior> superior = superiorRepo.findByFirstname(firstname);
        if (superior != null){
            return new ResponseEntity<>(superiorRepo.findByFirstname(firstname), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //search for all superiors with a specific lastname
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/lastname={lastname}"
    )
    public ResponseEntity<Collection<Superior>> getSuperiorByLastName(@PathVariable String lastname){
        Collection<Superior> superior = superiorRepo.findByLastname(lastname);
        if (superior != null){
            return new ResponseEntity<>(superiorRepo.findByLastname(lastname), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //add a new superior to the database
    @RequestMapping(
            method = RequestMethod.POST
    )
    public ResponseEntity<?> addSuperior(@RequestBody Superior superior){
        return new ResponseEntity<>(superiorRepo.save(superior), HttpStatus.OK);
    }

    //delete a superior with a specific id
    @RequestMapping(
            method = RequestMethod.DELETE,
            value = "/delte_id={id}"
    )
    public ResponseEntity<Void> deleteSuperiorById(@PathVariable long id) {
        superiorRepo.delete(id);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    //delete a superior with a specific email
    @RequestMapping(
            method = RequestMethod.DELETE,
            value = "/delete_email={email}"
    )
    public ResponseEntity<Void> deleteSuperiorByEmail(@PathVariable("email") String email) {
        Superior superior = superiorRepo.findByEmail(email);
        long tempID = superior.getId();

        if (superior == null) {
            return new ResponseEntity<Void>(HttpStatus.NOT_FOUND);
        }

        superiorRepo.delete(tempID);
        return new ResponseEntity<Void>(HttpStatus.OK);
    }
}
