package api.controllers;

import api.entities.Article;
import api.entities.Transaction;
import api.entities.Request.TransactionCreateRequest;
import api.entities.Request.TransactionValidateRequest;
import api.entities.Response.TransactionValidateResponse;
import api.repositories.TransactionRepository;
import api.services.SmartContractService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.web3j.abi.datatypes.generated.Int256;
import org.web3j.abi.datatypes.generated.Uint256;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/transaction")
public class TransactionController {

    @Autowired
    private TransactionRepository transactionRepo;

    @Autowired
    SmartContractService smartContractService;

    private Collection<TransactionCreateRequest> transactionMapper(Collection<Transaction> input) throws IOException {
        Collection<TransactionCreateRequest> temp = new ArrayList<TransactionCreateRequest>();
        for (Transaction e : input) {
            TransactionCreateRequest bla = new TransactionCreateRequest();
            bla.setCardnumber(e.getCardnumber());
            bla.setCompany(e.getCompany());
            bla.setPayload(e.getPayload());
            bla.setTransactionDate(e.getTransactiondate());
            bla.setShopname(e.getShopname());
            bla.setArticles(objectMapper.readValue(e.getArticles(),new TypeReference<List<Article>>(){}));
            temp.add(bla);
        }
        return temp;
    }

    //search for all transactions in database
    @RequestMapping(
            method = RequestMethod.GET
    )
    public ResponseEntity<Collection<TransactionCreateRequest>> getTransactions() throws IOException{
        Collection<Transaction> transactionList = transactionRepo.findAll();
        if (transactionList != null){
            return new ResponseEntity<Collection<TransactionCreateRequest>>(transactionMapper(transactionList), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //search all transactions by a specific cardnumber
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/cardnumber={cardnumber}"
    )
    public ResponseEntity<Collection<TransactionCreateRequest>> getTransactionByCardnumber(@PathVariable int cardnumber) throws IOException{
        Collection<Transaction> transactionList = transactionRepo.findByCardnumber(cardnumber);
        if (transactionList != null){
            return new ResponseEntity<Collection<TransactionCreateRequest>>(transactionMapper(transactionList), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //search for all transactions for a specific shopname
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/shopname={shopname}"
    )
    public ResponseEntity<Collection<TransactionCreateRequest>> getTransactionByShopname(@PathVariable String shopname) throws IOException{
        Collection<Transaction> transactionList = transactionRepo.findByShopname(shopname);
        if (transactionList != null) {
            return new ResponseEntity<Collection<TransactionCreateRequest>>(transactionMapper(transactionList), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //search for all transactions by a specific company
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/company={company}"
    )
    public ResponseEntity<Collection<TransactionCreateRequest>> getTransactionByComapnyname(@PathVariable String company) throws IOException{
        Collection<Transaction> transactionList = transactionRepo.findByCompany(company);
        if (transactionList != null) {
            return new ResponseEntity<Collection<TransactionCreateRequest>>(transactionMapper(transactionList), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    //search for company and cardnumber transactions
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/company={company}/cardnumber={cardnumber}"
    )
    public ResponseEntity<Collection<Transaction>> getTransactionByCompanyAndCardnumber(@PathVariable String company, @PathVariable long cardnumber){
        Collection<Transaction> transaction = transactionRepo.findByCompanyAndCardnumber(company, cardnumber);
        if (transaction != null){
            return new ResponseEntity<>(transactionRepo.findByCompanyAndCardnumber(company, cardnumber), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @RequestMapping(
            method = RequestMethod.POST,
            value = "/validate"
    )
    public ResponseEntity<TransactionValidateResponse> validateTransaction(@RequestBody TransactionValidateRequest transactionValidateRequest) throws Exception{
        Uint256 cardNumber = new Uint256(transactionValidateRequest.getCardnumber());
        int payLoad = transactionValidateRequest.getPayload();
        boolean validated = smartContractService.getContract().validateTransaction(cardNumber, new Int256(payLoad)).get().getValue();
        if (validated == false) {
            return new ResponseEntity<>(new TransactionValidateResponse(validated, "validation unsuccessful: payload to high"), HttpStatus.OK);
        }
        return new ResponseEntity<>(new TransactionValidateResponse(validated, "validation successful"), HttpStatus.OK);
    }

    private ObjectMapper objectMapper = new ObjectMapper();

    @RequestMapping(
            method = RequestMethod.POST,
            value = "/create"
    )
    public ResponseEntity<TransactionCreateRequest> createTransaction(@RequestBody TransactionCreateRequest transactionCreateRequest) throws Exception {
        java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
        int payLoad = transactionCreateRequest.getPayload();
        long cardNumber = transactionCreateRequest.getCardnumber();
        BigInteger newLimit =  smartContractService.getContract().getLimitCurrentByCardNumber(new Uint256(cardNumber)).get().getValue().subtract(new BigInteger( String.valueOf(payLoad) ));
        smartContractService.getContract().updateLimitCurrent(new Uint256(cardNumber), new Int256(newLimit)).get();
        Transaction transaction = new Transaction(
                payLoad,
                cardNumber,
                transactionCreateRequest.getShopname(),
                transactionCreateRequest.getCompany(),
                date,
                objectMapper.writeValueAsString(transactionCreateRequest.getArticles()));
        transactionRepo.save(transaction);
        return new ResponseEntity<>(transactionCreateRequest, HttpStatus.OK);
    }
}
