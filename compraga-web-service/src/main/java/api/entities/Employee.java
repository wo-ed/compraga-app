package api.entities;

import javax.persistence.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.codec.digest.DigestUtils;

@Entity
@Table(name = "employee")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    private String firstname;
    private String lastname;
    private String email;
    private String company;
    private String superioremail;
    private long cardnumber;
    private String image;

    public Employee(){

    }

    public Employee(long cardnumber){
        this.cardnumber = cardnumber;
    }

    public Employee(String firstname, String lastname, String email, String company, String superioremail, long cardnumber) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.company = company;
        this.superioremail = superioremail;
        this.cardnumber = cardnumber;
    }

    public Employee(String firstname, String lastname, String email, String company, String superioremail, long cardnumber, String image) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.company = company;
        this.superioremail = superioremail;
        this.cardnumber = cardnumber;
        this.image = image;
    }

//    public Employee(String firstname, String lastname, String email, String company, String superioremail, int cardnumber, String image) {
//        this.firstname = firstname;
//        this.lastname = lastname;
//        this.email = email;
//        this.company = company;
//        this.superioremail = superioremail;
//        this.cardnumber = cardnumber;
//        this.image = image;
//    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getSuperioremail() {
        return superioremail;
    }

    public void setSuperioremail(String superioremail) {
        this.superioremail = superioremail;
    }

    public long getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(long cardnumber) {
        this.cardnumber = cardnumber;
    }

    //public String getImagehash() throws NoSuchAlgorithmException {
    //    String hashed = DigestUtils
     //           .md5Hex(image).toUpperCase();
      //  return hashed;
    //}

//    public String getImagehash() {
//        return this.imagehash;
//    }
//
//    public void setImagehash(String imagehash) {
//        this.imagehash = imagehash;
//    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}

