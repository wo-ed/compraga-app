package api.entities;

import javax.persistence.*;

@Entity
@Table(name = "superior")
public class Superior {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    private String firstname;
    private String lastname;
    private String email;
    private String company;

    public Superior() {

    }

    public Superior(String firstname, String lastname, String email, String company){
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.company = company;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}

