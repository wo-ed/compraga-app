package api.entities.Request;

import api.entities.Article;
import api.entities.Transaction;

import java.sql.Date;
import java.util.List;

public class TransactionCreateRequest {

    private int payload;
    private long cardnumber;
    private String shopname;
    private String company;
    private List<Article> articles;
    private Date transactiondate;

    public TransactionCreateRequest() {

    }

    public int getPayload() {
        return payload;
    }

    public void setPayload(int payload) {
        this.payload = payload;
    }

    public long getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(long cardnumber) {
        this.cardnumber = cardnumber;
    }

    public String getShopname() {
        return shopname;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public Date getTransactiondate() {
        return transactiondate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactiondate = transactionDate;

    }
}
