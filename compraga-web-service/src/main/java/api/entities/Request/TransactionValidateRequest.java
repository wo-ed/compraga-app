package api.entities.Request;

public class TransactionValidateRequest {

    private int payload;
    private long cardnumber;

    public TransactionValidateRequest() {
    }

    public int getPayload() {
        return payload;
    }

    public void setPayload(int payload) {
        this.payload = payload;
    }

    public long getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(long cardnumber) {
        this.cardnumber = cardnumber;
    }
}
