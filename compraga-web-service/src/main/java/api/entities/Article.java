package api.entities;

/**
 * Created by romanskie on 04.07.17.
 */
public class Article {

    private String name;
    private int amount;
    private double price;

    public Article () {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
