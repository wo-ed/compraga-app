package api.entities.Response;

public class CardResponse {
    private long number;
    private String emailAddress;
    private int limitTransaction;
    private int limitCurrent;
    private int limitFix;
    private String imageHash;

    public CardResponse(long number, String emailAddress, int limitTransaction, int limitCurrent, int limitFix, String imageHash) {
        this.number = number;
        this.emailAddress = emailAddress;
        this.limitTransaction = limitTransaction;
        this.limitCurrent = limitCurrent;
        this.limitFix = limitFix;
        this.imageHash = imageHash;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public int getLimitTransaction() {
        return limitTransaction;
    }

    public void setLimitTransaction(int limitTransaction) {
        this.limitTransaction = limitTransaction;
    }

    public int getLimitCurrent() {
        return limitCurrent;
    }

    public void setLimitCurrent(int limitCurrent) {
        this.limitCurrent = limitCurrent;
    }

    public int getLimitFix() {
        return limitFix;
    }

    public void setLimitFix(int limitFix) {
        this.limitFix = limitFix;
    }

    public String getImageHash() {
        return imageHash;
    }

    public void setImageHash(String imageHash) {
        this.imageHash = imageHash;
    }

}
