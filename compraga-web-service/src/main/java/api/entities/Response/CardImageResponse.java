package api.entities.Response;

public class CardImageResponse {

    private String imageHash;

    public CardImageResponse(String imageHash) {
        this.imageHash = imageHash;
    }

    public String getImageHash() {
        return imageHash;
    }

    public void setImageHash(String imageHash) {
        this.imageHash = imageHash;
    }
}
