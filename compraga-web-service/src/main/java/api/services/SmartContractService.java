package api.services;

import api.contracts.TransactionValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.Wallet;
import org.web3j.crypto.WalletFile;
import org.web3j.protocol.ObjectMapperFactory;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.utils.Async;

import java.math.BigInteger;
import java.util.List;

@Service
public class SmartContractService {

    private static final BigInteger GAS_PRICE = BigInteger.valueOf(20_000_000_000L);
    private static final BigInteger GAS_LIMIT = BigInteger.valueOf(4_300_000);

    private TransactionValidator contract;

    public SmartContractService() throws Exception {

        Web3j web3j = Web3j.build(new HttpService("http://141.84.213.222:8546"));
        Resource resource = new ClassPathResource("walletfiles/UTC--2016-02-29T14-52-41.334222730Z--007ccffb7916f37f7aeef05e8096ecfbe55afc2f_password");
        //Resource resource = new ClassPathResource("walletfiles/UTC--2016-02-29T14-52-41.334222730Z--007ccffb7916f37f7aeef05e8096ecfbe55afc2f");
        ObjectMapper objectMapper = ObjectMapperFactory.getObjectMapper();
        WalletFile walletFile = objectMapper.readValue(resource.getInputStream(), WalletFile.class);
        //Credentials credentials = Credentials.create(Wallet.decrypt("", walletFile));
        Credentials credentials = Credentials.create(Wallet.decrypt("password", walletFile));

        String contractAddres = TransactionValidator.deploy(web3j, credentials, GAS_PRICE, GAS_LIMIT, BigInteger.ZERO).get().getContractAddress();
        contract = TransactionValidator.load(contractAddres, web3j, credentials, GAS_PRICE, GAS_LIMIT);
    }

    public TransactionValidator getContract() {
        return contract;
    }
}
