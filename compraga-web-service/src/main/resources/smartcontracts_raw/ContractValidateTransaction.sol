pragma solidity ^0.4.0;

contract TransactionValidator {
    
    uint numCards; //used to simulate the cardnumbers
    
    struct Card { //struct for cards, store unique cardnumbers, limits, image and mail of employee
        uint cardNumber;
        string employeeEmailAddress;
        int256 limitTransaction;
        int256 limitCurrent;
        int256 limitFix;
        string imageHash;
    }

    mapping(uint => Card) public cards; //map cards by uint identifier

    /* ====== get Methods ================================> */

        /*return all infos for a cardnumber*/
        function getCardByNumber(uint number) constant returns (uint cardNumber, string employeeEmailAddress, int256 limitTransaction, int256 limitCurrent, int256 limitFix, string imageHash){
            cardNumber = cards[number].cardNumber;
            employeeEmailAddress = cards[number].employeeEmailAddress;
            limitTransaction = cards[number].limitTransaction;
            limitCurrent = cards[number].limitCurrent;
            limitFix = cards[number].limitFix;
            imageHash = cards[number].imageHash;
        }

        /*return limitCurrent for a cardnumber*/
        function getLimitCurrentByCardNumber(uint number) constant returns (int256 limitCurrent) {
            limitCurrent = cards[number].limitCurrent;
        }

        /*return limitFix for a cardnumber*/
        function getLimitFixByCardNumber(uint number) constant returns (int256 limitFix) {
            limitFix = cards[number].limitFix;
        }

        /*return imageHash for a cardnumber*/
        function getImageHashByCardNumber(uint number) constant returns (string imageHash) {
            imageHash = cards[number].imageHash;
        }

    /* ====== update Methods ================================> */

        /*update an existing card with new data, give all data every time*/
        function updateCard(uint cardNumber, string employeeEmailAddress, int256 limitTransaction, int256 limitCurrent, int256 limitFix ,string imageHash){
            cards[cardNumber] = Card(cardNumber, employeeEmailAddress, limitTransaction, limitCurrent, limitFix, imageHash);
        }

        /* update the  of an existing card with a new limitCurrent */
        function updateLimitCurrent(uint cardNumber, int256 newlimitCurrent){
            Card oldState = cards[cardNumber];
            cards[cardNumber] = Card(oldState.cardNumber, oldState.employeeEmailAddress, oldState.limitTransaction, newlimitCurrent, oldState.limitFix, oldState.imageHash);
        }

    /* ====== helper Methods ================================> */

        /*create a card, give all data every time*/
        function createCard( uint cardNumber, string employeeEmailAddress, int256 limitTransaction, int256 limitCurrent, int256 limitFix, string imageHash){
            cards[cardNumber] = Card(cardNumber, employeeEmailAddress, limitTransaction, limitCurrent, limitFix, imageHash);
        }

        /*return if card exists or not*/
        function checkCardExistence(uint number) constant returns(bool) {
            if( cards[number].cardNumber==0 ) return false;
            else return true;
        }

        /*validate a transaction by checking payload against transactionlimit
        and limitCurrent */
        function validateTransaction(uint cardNumber, int256 payload) constant returns (bool) {
            if (cards[cardNumber].limitTransaction < payload) return (false);
            if (cards[cardNumber].limitCurrent < payload) return (false);
            else return (true);
        }
}