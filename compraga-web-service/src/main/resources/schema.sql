CREATE TABLE employee(
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  firstname VARCHAR (32),
  lastname VARCHAR (32),
  email VARCHAR (32),
  company VARCHAR (32),
  superioremail VARCHAR(64),
  cardnumber BIGINT,
  image VARCHAR(MAX),
);

CREATE TABLE superior(
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  firstname VARCHAR(32),
  lastname VARCHAR(32),
  email VARCHAR(64),
  company VARCHAR(32),
);

CREATE TABLE shop(
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(128),
  email VARCHAR(32),
);

CREATE TABLE transactions(
  id BIGINT PRIMARY KEY AUTO_INCREMENT,
  payload INT,
  cardnumber BIGINT,
  shopname VARCHAR(128),
  company VARCHAR(32),
  transactiondate DATE,
  articles VARCHAR(MAX),
);
